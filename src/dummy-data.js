export let emqs = [
  {
    _id: "6141c146f7c476901eb51d49",
    Description:
      "Each of the following clinical scenarios relates to management of delivery. For each clinical scenario, select the single most appropriate management option from the list above. Each option may be used once, more than once or not at all.",
    emq: {
      emq6: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option01",
        explanation: "Answer:\tA.\tMagnesium sulfate infusion. ",
        fileAMedia: "4.png",
        question:
          "4.\tA woman in her second pregnancy is admitted to the labour ward with severe pre-eclampsia at 29 weeks of gestation. She has commenced treatment with intravenous labetalol. Her uterine contraction frequency is about three every 10 minutes and the fetus is in cephalic presentation with the fetal head three-fifths palpable abdominally. A vaginal examination reveals a fully effaced and 4 cm dilated cervix?",
      },
      emq7: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option07",
        explanation:
          "Answer:\tG.\tTest for insulin-like growth factor-binding protein-1. ",
        fileAMedia: "5.png",
        question:
          "5.\tA 45-year-old woman, a mother of two children, has been admitted with symptoms suggestive of preterm rupture of membranes 12 hours ago at 29 weeks of gestation. She complains of intermittent abdominal pain. A sterile speculum examination of the vagina is inconclusive because of laxity of the vaginal wall, which obscures the view of the cervix. There is no obvious collection or pooling of amniotic fluid in the vagina. The uterus appears irritable and the fetus is in cephalic presentation.?",
      },
      emq8: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option13",
        explanation:
          "Answer:\tM.\tIntermittent auscultation of the fetal heartbeat. ",
        fileAMedia: "6.png",
        question:
          "6.\tA 29-year-old woman in her third pregnancy at 32+3 weeks of gestation is in labour following the spontaneous onset of uterine contractions 2 days ago. Her pregnancy had been uneventful until now.. A vaginal examination reveals intact membranes, and a fully effaced and 7 cm dilated cervix with the fetal head at 0 station. Her body mass index (BMI) is 32 kg/m2 and the CTG has been reassuring since admission. In the last hour, it has become difficult to interpret the CTG because of difficulty in monitoring the fetal heartbeat due to many episodes of loss of contact?",
      },
    },
    exp: "",
    options: {
      option01: "Magnesium sulfate infusion",
      option02: "Nitrazine test",
      option03: "Nifedipine for tocolysis",
      option04: "Rescue cervical cerclage",
      option05: "Induction of labour",
      option06: "Ritodrine infusion",
      option07:
        "Test for insulin-like growth factor-binding protein-1 (IGFBP-1)",
      option08: "Ultrasound scan",
      option09: "Attach fetal scalp electrode",
      option10: "Delivery by caesarean section",
      option11: "Digital examination of the vagina and cervix",
      option12: "Fetal scalp blood sampling",
      option13: "Intermittent auscultation of the fetal heartbeat",
    },
    questiontype: "2",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51ce8",
    Description:
      "For each of the following clinical descriptions, what is the most likely diagnosis from the option list above? Each option may be used once, more than once or not at all.",
    aName: "",
    emq: {
      emq9: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option10",
        explanation: "Answer:\tJ.\tLichen planus. ",
        fileAMedia: "7.png",
        question:
          "7.\tA 78-year-old woman presents with vulval irritation and soreness. On examination the vulva is red in colour, slightly oedematous and there are small, red papules scattered randomly beyond the perimeter of the vulva. She also complains of soreness and irritation under the breasts.",
      },
      emq10: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option12",
        explanation: "Answer:\tL.\tTransfer to intensive treatment unit.",
        fileAMedia: "8.png",
        question:
          "8.\tA 20-year-old primigravida had a normal delivery of a live infant 12 hours previously. She has developed severe gestational proteinuric hypertension, her clotting is normal, serum albumin is 43 g/dl, there is no ankle clonus and her blood pressure is 160/100 mmHg. She has been given one litre of Hartmann’s solution intravenously since her delivery and has been anuric. The central venous pressure is +10 mmHg, serum sodium 132 mmol/l, serum potassium 7.1 mmol/l and serum urea 22 mmol/l.",
      },
      emq11: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option06",
        explanation: "Answer:\tF.\tIntravenous magnesium sulphates.\t",
        fileAMedia: "9.png",
        question:
          "9.\tA 20-year-old primigravida delivered a live infant 24 hours previously. She has developed severe gestational proteinuric hypertension. Treatment with intravenous magnesium was required. Her fluid balance is satisfactory and serum urea, electrolytes and clotting profile are all normal. Her respiratory rate falls to 6 per minute and she is drowsy but rousable.’",
      },
    },
    exp: "",
    options: {
      option01: "Psoriasis",
      option02: "Recurrent herpes infection",
      option03: "Systemic lupus erythematosus",
      option04: "Vulval intraepithelial neoplasia (VIN)",
      option05: "Vulvodynia",
      option06: "Intravenous magnesium sulphate",
      option07: "Chronic vulvovaginal candidiasis",
      option08: "Eczema",
      option09: "Idiopathic",
      option10: "Lichen planus",
      option11: "Lichen sclerosus",
      option12: "Transfer to intensive treatment unit",
      option13: "Paget’s disease",
    },
    questiontype: "2",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51ce9",
    Description:
      "For each of the following clinical scenarios, choose the single most likely diagnosis from the list of options above. Each option may be used once, more than once or not at all.",
    aName: "",
    emq: {
      emq12: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option14",
        explanation: "Answer:\tN.\tUterine perforation.",
        fileAMedia: "11.png",
        question:
          "10.\tA 52-year-old woman with frequent heavy periods is listed for diagnostic hysteroscopy. She has had two children, both delivered by caesarean section. She is hypertensive and her BMI is 26.",
      },
      emq13: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option10",
        explanation: "Answer:\tJ.\tPain.",
        fileAMedia: "10.png",
        question:
          "11.\tA 56-year-old woman is scheduled for laparotomy and possible bilateral salpingoophorectomy for an ovarian mass. She had a total abdominal hysterectomy at the age of 40 for fibroids and is in discomfort with an ovarian mass which measures 15 cm in diameter on ultrasound examination.\t",
      },
    },
    exp: "",
    options: {
      option01: "Damage to bladder/ureter",
      option02: "Damage to bowel",
      option03: "Failure rate 1 in 100",
      option04: "Failure to gain entry into abdominal cavity",
      option05: "Failure to identify disease",
      option06: "Failure to visualise uterine cavity",
      option07: "Haemorrhage requiring blood transfusion",
      option08: "Haemorrhage requiring return to theatre",
      option09: "Laparotomy",
      option10: "Pain",
      option11: "Premature menopause",
      option12: "Removal of ovaries",
      option13: "Urinary retention",
      option14: "Uterine perforation",
    },
    questiontype: "2",
    score: 1,
  },
]

export const questions = [
  {
    _id: "6141c146f7c476901eb51d48",
    Description:
      "For each case described below, choose the single most likely cause of maternal death from the above list of options. Each option may be used once, more than once, or not at all.",
    emq: {
      emq3: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option02",
        explanation: "Answer:\tB.\tCardiomyopathy.",
        fileAMedia: "1.png",
        question:
          "1.\tA previously healthy 18-year-old primigravida presents at 36 weeks feeling unwell and tired. Her brother died unexpectedly aged 19 years. Her CXR showed an enlarged heart. While being admitted she developed increasing shortness of breath and died despite intensive resuscitation.?",
      },
      emq4: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option03",
        explanation:
          "Answer:\tC.\tChest infection, branch of the uterine artery.",
        fileAMedia: "2.png",
        question:
          "2.\tA 30-year-old woman, 28 weeks of gestation in her sixth pregnancy, presents to A&E with breathlessness and displays severe anxiety. She had complained of left-sided pelvic pain for a week. While being assessed she collapsed and it was not possible to resuscitate her.",
      },
      emq5: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option06",
        explanation: "Answer:\tF.\tHaemorrhage.",
        fileAMedia: "3.png",
        question:
          "3.\tA 23-year-old woman presents with a two-year history of vulval, perineal and perianal irritation. The vulva is red, excoriated and there areas of white, thickened skin. Application of 3% acetic acid shows areas of mosaic and coarse punctuation.",
      },
    },
    exp: "",
    options: {
      option01: "Amniotic fluid embolism",
      option02: "Cardiomyopathy",
      option03: "Chest infection",
      option04: "CVA",
      option05: "Endocarditis",
      option06: "Haemorrhage",
      option07: "HELLP syndrome",
      option08: "Myocardial infarction",
      option09: "Placental abruption",
      option10: "Placenta praevia",
      option11: "Pulmonary embolism",
      option12: "Pulmonary hypertension",
      option13: "Sepsis",
      option14: "Substance misuse",
    },
    questiontype: "2",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dba",
    Description: "Q1: Where is the train station?",
    answer: {
      option03: "Wales",
    },
    options: {
      option01: "Finland",
      option02: "Czech Republic",
      option03: "Wales",
      option04: "Moldova",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbb",
    Description:
      "Q2: Which company did Valve cooperate with in the creation of the Vive? ",
    answer: {
      option02: "HTC",
    },
    options: {
      option01: "Oculus",
      option02: "HTC",
      option03: "Google",
      option04: "Razer",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbc",
    Description:
      "Q3: What was the name of the WWF professional wrestling tag team made up of the wrestlers Ax and Smash?",
    answer: {
      option01: "Demolition",
    },
    options: {
      option01: "Demolition",
      option02: "The Dream Team",
      option03: "The Bushwhackers",
      option04: "The British Bulldogs",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbd",
    Description: "Q4: What is the first book of the Old Testament?",
    answer: {
      option02: "Genesis",
    },
    options: {
      option01: "Exodus",
      option02: "Genesis",
      option03: "Leviticus",
      option04: "Numbers",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbe",
    Description:
      "Q5: In the video-game franchise Kingdom Hearts, the main protagonist, carries a weapon with what shape?",
    answer: {
      option02: "Key",
    },
    options: {
      option01: "Sword",
      option02: "Key",
      option03: "Pen",
      option04: "Cellphone",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbf",
    Description:
      "Q6: Which best selling toy of 1983 caused hysteria, resulting in riots breaking out in stores?",
    answer: {
      option01: "Cabbage Patch Kids",
    },
    options: {
      option01: "Cabbage Patch Kids",
      option02: "Transformers",
      option03: "Care Bears",
      option04: "Rubiks Cube",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc0",
    Description: "Q7: What does a funambulist walk on?",
    answer: {
      option02: "A Tight Rope",
    },
    options: {
      option01: "Broken Glass",
      option02: "A Tight Rope",
      option03: "Balls",
      option04: "The Moon",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc1",
    Description:
      "Q8: In past times, what would a gentleman keep in his fob pocket?",
    answer: {
      option04: "Watch",
    },
    options: {
      option01: "Money",
      option02: "Keys",
      option03: "Notebook",
      option04: "Watch",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc2",
    Description:
      "Q9: What does the S stand for in the abbreviation SIM, as in SIM card?",
    answer: {
      option01: "Subscriber",
    },
    options: {
      option01: "Subscriber",
      option02: "Single",
      option03: "Secure",
      option04: "Solid",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc3",
    Description:
      "Q10: What word represents the letter &#039;T&#039; in the NATO phonetic alphabet?",
    answer: {
      option01: "Tango",
    },
    options: {
      option01: "Tango",
      option02: "Target",
      option03: "Taxi",
      option04: "Turkey",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc4",
    Description: "Q11: Which American president appears on a one dollar bill?",
    answer: {
      option01: "George Washington",
    },
    options: {
      option01: "George Washington",
      option02: "Thomas Jefferson",
      option03: "Abraham Lincoln",
      option04: "Benjamin Franklin",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc5",
    Description:
      "Q12: What is the shape of the toy invented by Hungarian professor Ernő Rubik?",
    answer: {
      option04: "Cube",
    },
    options: {
      option01: "Sphere",
      option02: "Cylinder",
      option03: "Pyramid",
      option04: "Cube",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc6",
    Description:
      "Q13: Which one of the following rhythm games was made by Harmonix?",
    answer: {
      option01: "Rock Band",
    },
    options: {
      option01: "Rock Band",
      option02: "Meat Beat Mania",
      option03: "Guitar Hero Live",
      option04: "Dance Dance Revolution",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc7",
    Description: "Q14: What is the nickname of the US state of California?",
    answer: {
      option02: "Golden State",
    },
    options: {
      option01: "Sunshine State",
      option02: "Golden State",
      option03: "Bay State",
      option04: "Treasure State",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc8",
    Description:
      "Q15: Which one of these is not a typical European sword design?",
    answer: {
      option01: "Scimitar",
    },
    options: {
      option01: "Scimitar",
      option02: "Falchion",
      option03: "Ulfberht",
      option04: "Flamberge",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc9",
    Description:
      "Q16: According to Sherlock Holmes, If you eliminate the impossible, whatever remains, however improbable, must be the...?",
    answer: {
      option03: "Truth",
    },
    options: {
      option01: "Answer",
      option02: "Cause",
      option03: "Truth",
      option04: "Source",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dca",
    Description:
      "Q17: What machine element is located in the center of fidget spinners?",
    answer: {
      option01: "Bearings",
    },
    options: {
      option01: "Bearings",
      option02: "Axles",
      option03: "Gears",
      option04: "Belts",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dcb",
    Description:
      "Q18: Which sign of the zodiac comes between Virgo and Scorpio?",
    answer: {
      option01: "Libra",
    },
    options: {
      option01: "Libra",
      option02: "Gemini",
      option03: "Taurus",
      option04: "Capricorn",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dcc",
    Description: "Q19: What is the Zodiac symbol for Gemini?",
    answer: {
      option01: "Twins",
    },
    options: {
      option01: "Twins",
      option02: "Fish",
      option03: "Scales",
      option04: "Maiden",
    },
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dcd",
    Description: "Q20: What zodiac sign is represented by a pair of scales?",
    answer: {
      option04: "Libra",
    },
    options: {
      option01: "Aries",
      option02: "Capricorn",
      option03: "Sagittarius",
      option04: "Libra",
    },
    score: 1,
  },
]
export const answer = [
  {
    _id: "6141c146f7c476901eb51dba",
    Description: "Q1: Where is the train station?",
    answer: {
      option03: "Wales",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",
    options: {
      option01: "Finland",
      option02: "Czech Republic",
      option03: "Wales",
      option04: "Moldova",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbb",
    Description:
      "Q2: Which company did Valve cooperate with in the creation of the Vive? ",
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",
    answer: {
      option02: "HTC",
    },
    options: {
      option01: "Oculus",
      option02: "HTC",
      option03: "Google",
      option04: "Razer",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51d48",
    Description:
      "For each case described below, choose the single most likely cause of maternal death from the above list of options. Each option may be used once, more than once, or not at all.",
    emq: {
      emq3: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option02",
        explanation: "Answer:\tB.\tCardiomyopathy.",
        fileAMedia: "1.png",
        question:
          "1.\tA previously healthy 18-year-old primigravida presents at 36 weeks feeling unwell and tired. Her brother died unexpectedly aged 19 years. Her CXR showed an enlarged heart. While being admitted she developed increasing shortness of breath and died despite intensive resuscitation.?",
      },
      emq4: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option03",
        explanation:
          "Answer:\tC.\tChest infection, branch of the uterine artery.",
        fileAMedia: "2.png",
        question:
          "2.\tA 30-year-old woman, 28 weeks of gestation in her sixth pregnancy, presents to A&E with breathlessness and displays severe anxiety. She had complained of left-sided pelvic pain for a week. While being assessed she collapsed and it was not possible to resuscitate her.",
      },
      emq5: {
        AMediaType: "image/png",
        AMediaURL:
          "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
        answer: "option06",
        explanation: "Answer:\tF.\tHaemorrhage.",
        fileAMedia: "3.png",
        question:
          "3.\tA 23-year-old woman presents with a two-year history of vulval, perineal and perianal irritation. The vulva is red, excoriated and there areas of white, thickened skin. Application of 3% acetic acid shows areas of mosaic and coarse punctuation.",
      },
    },
    exp: "",
    options: {
      option01: "Amniotic fluid embolism",
      option02: "Cardiomyopathy",
      option03: "Chest infection",
      option04: "CVA",
      option05: "Endocarditis",
      option06: "Haemorrhage",
      option07: "HELLP syndrome",
      option08: "Myocardial infarction",
      option09: "Placental abruption",
      option10: "Placenta praevia",
      option11: "Pulmonary embolism",
      option12: "Pulmonary hypertension",
      option13: "Sepsis",
      option14: "Substance misuse",
    },
    questiontype: "2",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbc",
    Description:
      "Q3: What was the name of the WWF professional wrestling tag team made up of the wrestlers Ax and Smash?",
    answer: {
      option01: "Demolition",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Demolition",
      option02: "The Dream Team",
      option03: "The Bushwhackers",
      option04: "The British Bulldogs",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbd",
    Description: "Q4: What is the first book of the Old Testament?",
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    answer: {
      option02: "Genesis",
    },
    options: {
      option01: "Exodus",
      option02: "Genesis",
      option03: "Leviticus",
      option04: "Numbers",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbe",
    Description:
      "Q5: In the video-game franchise Kingdom Hearts, the main protagonist, carries a weapon with what shape?",
    answer: {
      option02: "Key",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Sword",
      option02: "Key",
      option03: "Pen",
      option04: "Cellphone",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dbf",
    Description:
      "Q6: Which best selling toy of 1983 caused hysteria, resulting in riots breaking out in stores?",
    answer: {
      option01: "Cabbage Patch Kids",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Cabbage Patch Kids",
      option02: "Transformers",
      option03: "Care Bears",
      option04: "Rubiks Cube",
    },
    questiontype: "1",
    score: 1,
  },
  {
    _id: "6141c146f7c476901eb51dc0",
    Description: "Q7: What does a funambulist walk on?",
    answer: {
      option02: "A Tight Rope",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Broken Glass",
      option02: "A Tight Rope",
      option03: "Balls",
      option04: "The Moon",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc1",
    Description:
      "Q8: In past times, what would a gentleman keep in his fob pocket?",
    answer: {
      option04: "Watch",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Money",
      option02: "Keys",
      option03: "Notebook",
      option04: "Watch",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc2",
    Description:
      "Q9: What does the S stand for in the abbreviation SIM, as in SIM card?",
    answer: {
      option01: "Subscriber",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Subscriber",
      option02: "Single",
      option03: "Secure",
      option04: "Solid",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc3",
    Description:
      "Q10: What word represents the letter &#039;T&#039; in the NATO phonetic alphabet?",
    answer: {
      option01: "Tango",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Tango",
      option02: "Target",
      option03: "Taxi",
      option04: "Turkey",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc4",
    Description: "Q11: Which American president appears on a one dollar bill?",
    answer: {
      option01: "George Washington",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "George Washington",
      option02: "Thomas Jefferson",
      option03: "Abraham Lincoln",
      option04: "Benjamin Franklin",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc5",
    Description:
      "Q12: What is the shape of the toy invented by Hungarian professor Ernő Rubik?",
    answer: {
      option04: "Cube",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Sphere",
      option02: "Cylinder",
      option03: "Pyramid",
      option04: "Cube",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc6",
    Description:
      "Q13: Which one of the following rhythm games was made by Harmonix?",
    answer: {
      option01: "Rock Band",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Rock Band",
      option02: "Meat Beat Mania",
      option03: "Guitar Hero Live",
      option04: "Dance Dance Revolution",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc7",
    Description: "Q14: What is the nickname of the US state of California?",
    answer: {
      option02: "Golden State",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Sunshine State",
      option02: "Golden State",
      option03: "Bay State",
      option04: "Treasure State",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc8",
    Description:
      "Q15: Which one of these is not a typical European sword design?",
    answer: {
      option01: "Scimitar",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Scimitar",
      option02: "Falchion",
      option03: "Ulfberht",
      option04: "Flamberge",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dc9",
    Description:
      "Q16: According to Sherlock Holmes, If you eliminate the impossible, whatever remains, however improbable, must be the...?",
    answer: {
      option03: "Truth",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Answer",
      option02: "Cause",
      option03: "Truth",
      option04: "Source",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dca",
    Description:
      "Q17: What machine element is located in the center of fidget spinners?",
    answer: {
      option01: "Bearings",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Bearings",
      option02: "Axles",
      option03: "Gears",
      option04: "Belts",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dcb",
    Description:
      "Q18: Which sign of the zodiac comes between Virgo and Scorpio?",
    answer: {
      option01: "Libra",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Libra",
      option02: "Gemini",
      option03: "Taurus",
      option04: "Capricorn",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dcc",
    Description: "Q19: What is the Zodiac symbol for Gemini?",
    answer: {
      option01: "Twins",
    },
    exp: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",

    options: {
      option01: "Twins",
      option02: "Fish",
      option03: "Scales",
      option04: "Maiden",
    },
    score: 1,
    questiontype: "1",
  },
  {
    _id: "6141c146f7c476901eb51dcd",
    Description: "Q20: What zodiac sign is represented by a pair of scales?",
    answer: {
      option04: "Libra",
    },
    exp: "https://dummyimage.com/600x400/000/fff",
    options: {
      option01: "Aries",
      option02: "Capricorn",
      option03: "Sagittarius",
      option04: "Libra",
    },
    score: 1,
    questiontype: "1",
  },
]
