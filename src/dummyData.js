export const props = {
  length: 24,
  action: "PUSH",
  location: {
    pathname: "/new-mock",
    state: {
      attemptKey: null,
      type: 0,
      courseId: "61499df685492a770d84a1a0",
      courseType: 0,
      userId: "618cae4ae2ea34c41746e8ea",
      data: {
        topicId: 1,
        exam: [
          {
            _id: "6141c146f7c476901eb51d48",
            firebaseKey: "-MF_i8fJ1FekV5ebkSdR",
            Description:
              "Each of the following clinical scenarios relates to a pelvic surgical operative procedure. For each operative step, select the single most applicable option from the above list. Each option may be used once, more than once or not at all.",
            aName: "",
            emq: {
              emq3: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://firebasestorage.googleapis.com/v0/b/medexpert-d7560.appspot.com/o/Media%2FQuestions%2Fa0b12f03-a1c0-4266-81b1-b9855a64097b.png?alt=media&token=6575f0f7-c9cf-4207-b4ad-a4b86dc2121b",
                answer: "option12",
                explanation:
                  "Answer:\tL.\tSuperior gluteal artery\n\nREFERENCE:\tStandring S. (ed.) Gray’s Anatomy, 40th edn. Churchill Livingstone,2008.\n,",
                fileAMedia: "1.png",
                question:
                  "1.\tA surgeon is demonstrating the anatomy of a pelvic blood vessel to a trainee. The surgeon traces the course of the internal iliac artery and identifies the largest branch. Which artery is this?",
              },
              emq4: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://firebasestorage.googleapis.com/v0/b/medexpert-d7560.appspot.com/o/Media%2FQuestions%2Fd30661c7-e1b3-457e-b377-813e6c9a6c72.png?alt=media&token=c6b30088-2e8c-49fa-9b6b-95c5568d4db5",
                answer: "option03",
                explanation:
                  "Answer:\tC.\tDescending cervical artery, branch of the uterine artery\n\nREFERENCE:\tStandring S. (ed.) Gray’s Anatomy, 40th edn. Churchill Livingstone, 2008.\n,",
                fileAMedia: "2.png",
                question:
                  "2.\tA 25-year-old woman has just undergone a knife cone biopsy of her cervix. Both angles of the cervix are bleeding profusely. A suture needs to be inserted to arrest this bleeding. Which blood vessel is the main contributor to this bleeding?\t",
              },
              emq5: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://firebasestorage.googleapis.com/v0/b/medexpert-d7560.appspot.com/o/Media%2FQuestions%2F695f7392-c208-4f1b-8247-4be8d9ce72ec.png?alt=media&token=ae69dabc-e48f-4cd0-b265-da16f740db9e",
                answer: "option06",
                explanation:
                  "Answer:\tF.\tInferior epigastric artery, branch of the external iliac artery\n\nREFERENCE:\tStandring S. (ed.) Gray’s Anatomy, 40th edn. Churchill Livingstone, 2008.\n,",
                fileAMedia: "3.png",
                question:
                  " 3.\tDuring a laparoscopic salpingectomy for ectopic pregnancy, the surgeon is about to introduce a lateral laparoscopic port on the left abdominal flank. The surgeon is trying to determine the correct position to make the incision and is looking for the course of a blood vessel. Which blood vessel is this?",
              },
            },
            exp: "",
            key: "-MF_i8fJ1FekV5ebkSdR",
            lastUpdated: "2020-08-25T16:09:00+03:00",
            media: "",
            mediaE: "",
            mediaType: 1,
            options: {
              option01: "Vaginal artery, branch of the internal iliac artery",
              option02:
                "Descending cervical artery, branch of the internal iliac artery",
              option03:
                "Descending cervical artery, branch of the uterine artery ",
              option04: "Inferior gluteal artery",
              option05: "Internal pudendal artery",
              option06:
                "Inferior epigastric artery, branch of the external iliac artery ",
              option07:
                "Inferior epigastric artery, branch of the internal iliac artery ",
              option08: "Ovarian artery, branch of the abdominal aorta",
              option09: "Ovarian artery, branch of the internal iliac artery",
              option10:
                "Superior epigastric artery, branch of the internal thoracic artery",
              option11: "Superior vesical artery",
              option12: "Superior gluteal artery",
              option13:
                "Vaginal artery, branch of the descending cervical artery",
              option14: "Azygos artery of the vagina",
            },
            qName: "",
            type: "3",
            year: "",
            parentTag: [
              {
                tagId: "M2 Free Mocks",
              },
            ],
            childTag: [
              {
                tagId: "M2 Free Mock - 001 EMQ",
              },
            ],
            score: 1,
          },
          {
            _id: "6141c146f7c476901eb51ce8",
            Description:
              "For each of the following clinical descriptions, what is the most likely diagnosis from the option list above? Each option may be used once, more than once or not at all.",
            aName: "",
            emq: {
              emq9: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
                answer: "option10",
                explanation: "Answer:\tJ.\tLichen planus. ",
                fileAMedia: "7.png",
                question:
                  "7.\tA 78-year-old woman presents with vulval irritation and soreness. On examination the vulva is red in colour, slightly oedematous and there are small, red papules scattered randomly beyond the perimeter of the vulva. She also complains of soreness and irritation under the breasts.",
              },
              emq10: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
                answer: "option12",
                explanation:
                  "Answer:\tL.\tTransfer to intensive treatment unit.",
                fileAMedia: "8.png",
                question:
                  "8.\tA 20-year-old primigravida had a normal delivery of a live infant 12 hours previously. She has developed severe gestational proteinuric hypertension, her clotting is normal, serum albumin is 43 g/dl, there is no ankle clonus and her blood pressure is 160/100 mmHg. She has been given one litre of Hartmann’s solution intravenously since her delivery and has been anuric. The central venous pressure is +10 mmHg, serum sodium 132 mmol/l, serum potassium 7.1 mmol/l and serum urea 22 mmol/l.",
              },
              emq11: {
                AMediaType: "image/png",
                AMediaURL:
                  "https://image.shutterstock.com/image-photo/construction-worker-control-pouring-concrete-600w-752076598.jpg",
                answer: "option06",
                explanation: "Answer:\tF.\tIntravenous magnesium sulphates.\t",
                fileAMedia: "9.png",
                question:
                  "9.\tA 20-year-old primigravida delivered a live infant 24 hours previously. She has developed severe gestational proteinuric hypertension. Treatment with intravenous magnesium was required. Her fluid balance is satisfactory and serum urea, electrolytes and clotting profile are all normal. Her respiratory rate falls to 6 per minute and she is drowsy but rousable.’",
              },
            },
            exp: "",
            options: {
              option01: "Psoriasis",
              option02: "Recurrent herpes infection",
              option03: "Systemic lupus erythematosus",
              option04: "Vulval intraepithelial neoplasia (VIN)",
              option05: "Vulvodynia",
              option06: "Intravenous magnesium sulphate",
              option07: "Chronic vulvovaginal candidiasis",
              option08: "Eczema",
              option09: "Idiopathic",
              option10: "Lichen planus",
              option11: "Lichen sclerosus",
              option12: "Transfer to intensive treatment unit",
              option13: "Paget’s disease",
            },
            type: "3",
            score: 1,
          },
          {
            _id: "6141c146f7c476901eb51dba",
            Description: "Q1: Where is the train station?",
            answer: {
              option03: "Wales",
            },
            options: {
              option01: "Finland",
              option02: "Czech Republic",
              option03: "Wales",
              option04: "Moldova",
            },
            score: 1,
            type: "1",
          },
          {
            _id: "6141c146f7c476901eb51dbb",
            Description:
              "Q2: Which company did Valve cooperate with in the creation of the Vive? ",
            answer: {
              option02: "HTC",
            },
            options: {
              option01: "Oculus",
              option02: "HTC",
              option03: "Google",
              option04: "Razer",
            },
            exp: "Answer:\tL.\tSuperior gluteal artery\n\nREFERENCE:\tStandring S. (ed.) Gray’s Anatomy, 40th edn. Churchill Livingstone,2008.\n,",
            score: 1,
            type: "1",
          },
          {
            _id: "6141c146f7c476901eb51dbc",
            Description:
              "Q3: What was the name of the WWF professional wrestling tag team made up of the wrestlers Ax and Smash?",
            answer: {
              option01: "Demolition",
            },
            options: {
              option01: "Demolition",
              option02: "The Dream Team",
              option03: "The Bushwhackers",
              option04: "The British Bulldogs",
            },
            score: 1,
            type: "1",
          },
        ],
        examDetails: {
          info: "",
          instructions:
            "1.\tManage time and comly done before closing your device\n,",
          isShuffle: false,
          max_score: 5,
          max_time: "30",
          min_score: 60,
          title: "Mock Exam - 001 EMQs",
        },
      },
      paperKey: 1,
      categories: [
        {
          catId: "MRCOG",
        },
        {
          catId: "MRCOG-2",
        },
      ],
      sectionName: "MRCOG-2 Mock Exams",
      sectionKey: "section0",
      courseTitle: "MRCOG-2 Free Mock Exams",
    },
    search: "",
    hash: "",
    key: "b664gv",
  },
}
