import React, { useState, useEffect } from "react"
import { makeStyles } from "@mui/styles"
import Navbar from "./component/AnswerComponent/Navbar"
import Question from "./component/AnswerComponent/Question"
import QuestionBank from "./component/AnswerComponent/QuestionBank"
import ProgressBar from "./component/AnswerComponent/ProgessBar"

const useStyles = makeStyles({
  main: {
    padding: "0px 20px",
    marginTop: "30px",
    backgroundColor: "#fafafa",
    display: "grid",
    gridTemplateColumns: "1fr 4fr",
    "@media (max-width: 1000px)": {
      gridTemplateColumns: "350px auto",
      columnGap: "30px",
    },
    "@media (max-width: 850px)": {
      gridTemplateColumns: "260px auto",
      columnGap: "20px",
    },
    "@media (max-width: 650px)": {
      gridTemplateColumns: "1fr",
      columnGap: "20px",
    },
    "@media (max-width: 370px)": {
      gridTemplateColumns: "300px",
    },
    columnGap: "50px",
  },
})
function App({ newData, setNewData, setRouteAnswer, propsData }) {
  const classes = useStyles()
  const [selected, setSelected] = useState(null)
  const [result, setResult] = useState(false)
  const [retake, setRetake] = useState(true)
  const [total, setTotal] = useState({
    totalScore: 0,
    totalLength: 0,
  })

  useEffect(() => {
    if (retake) {
      const newTotal = newData.myAttempts.attempted.reduce(
        (total, data) => {
          total.totalScore += data.score
          total.totalLength += 1
          return total
        },
        {
          totalScore: 0,
          totalLength: 0,
        }
      )
      setNewData((data) => {
        const newData = data.myAttempts.data.map((alldata) => {
          const [newAttempted] = data.myAttempts.attempted.filter((dataa) => {
            return alldata._id === dataa.id
          })
          if (alldata.type === "3") {
            const updatedAnswered = Object.keys(alldata.emq).map((emqss) => {
              const answers = Object.values(alldata.emq[emqss])[2]
              return { [emqss]: answers }
            })
            const realanswer = newAttempted.emq.map((emqs) => {
              const [answered] = updatedAnswered.filter((ansemq) => {
                return emqs.id === Object.keys(ansemq)[0]
              })
              return {
                ...emqs,
                realanswer: Object.values(answered)[0],
              }
            })
            return {
              ...newAttempted,
              emq: realanswer,
            }
          }
          return { ...newAttempted, realanswer: Object.keys(alldata.answer)[0] }
        })
        return {
          ...data,
          myAttempts: {
            ...data.myAttempts,
            attempted: newData,
          },
        }
      })
      setTotal(newTotal)
      setSelected(newData.myAttempts.attempted[0].id)
      setRetake(false)
    }
  }, [retake, newData])

  useEffect(() => {
    if (newData) {
      const result = newData.myAttempts.data.reduce(
        (total, data) => {
          if (data.type === "3") {
            newData.myAttempts.attempted.map((attemptedData) => {
              if (attemptedData.id === data._id) {
                Object.keys(data.emq).map((emqss, index) => {
                  if (
                    attemptedData.emq[index].ansId === data.emq[emqss].answer
                  ) {
                    total.correct += 1
                  } else {
                    total.incorrect += 1
                  }
                  if (attemptedData.emq[index].status === "unanswered") {
                    total.unanswered += 1
                  }
                  total.total += 1
                })
                const isTrue = Object.keys(data.emq).every((emqss, index) => {
                  return (
                    attemptedData.emq[index].ansId === data.emq[emqss].answer
                  )
                })
                if (isTrue) {
                  total.totalScore += 1
                }
              }
            })
          }
          if (data.type === "1") {
            newData.myAttempts.attempted.map((attemptedData) => {
              if (attemptedData.id === data._id) {
                if (Object.keys(data.answer)[0] === attemptedData.ansId) {
                  total.correct += 1
                  total.totalScore += data.score
                } else {
                  total.incorrect += 1
                }
                if (
                  attemptedData.status === "unanswered" ||
                  attemptedData.status === "skipped"
                ) {
                  total.unanswered += 1
                }
              }
            })
            total.total += 1
          }
          return total
        },
        { unanswered: 0, correct: 0, incorrect: 0, total: 0, totalScore: 0 }
      )
      setResult(result)
    }
  }, [newData])

  if (!newData) {
    return <h3>Loading. . .</h3>
  }
  if (!result) {
    return <h4>Result loading ...</h4>
  }
  return (
    <>
      <Navbar setNewData={setNewData} setRouteAnswer={setRouteAnswer} />
      <div className={classes.main}>
        <QuestionBank
          questionLength={total.totalLength}
          question={newData.myAttempts.attempted}
          setSelected={setSelected}
          selected={selected}
        />
        <div className={classes.questionsection}>
          <ProgressBar result={result} />
          <Question
            question={newData.myAttempts.data}
            setSelected={setSelected}
            attemptedArray={newData.myAttempts.attempted}
            questionLength={total.totalLength}
            selected={selected}
            setNewData={setNewData}
            newData={newData}
          />
        </div>
      </div>
    </>
  )
}

export default App
