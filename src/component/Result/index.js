import React from "react"
import Backdrop from "@mui/material/Backdrop"
import Box from "@mui/material/Box"
import Modal from "@mui/material/Modal"
import Fade from "@mui/material/Fade"
import Table from "./Table"
import Chart from "./Chart"
import { Divider, Button } from "@mui/material"
import { Link } from "react-router-dom"
import { useStyles, style } from "./style"

export default function TransitionsModal({
  open,
  setOpen,
  result,
  setRetake,
  setRouteAnswer,
  setNewData,
}) {
  const classes = useStyles()
  const Parcentage = (result.correct / result.total) * 100
  const handleRetake = () => {
    setOpen(false)
    setRetake(true)
    setNewData(null)
  }

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <div className={classes.navbar}>
              <h2>Exam Summary</h2>
            </div>
            <div className={classes.main}>
              <div className={classes.header}>
                <h2> Give it another Try </h2>
                <h4>You have finished your exam</h4>
              </div>
              <div className={classes.grid}>
                <div>
                  <Chart result={result} />
                </div>
                <div>
                  <Table result={result} />
                </div>
              </div>
            </div>
            <Divider />
            <div className={classes.result}>
              <h3>Score : {result.totalScore} </h3>
              <h3>Parcentage : {Parcentage.toString().substring(0, 4)} % </h3>
            </div>
            <Divider />
            <div className={classes.btn}>
              <Button
                variant="contained"
                color="error"
                style={{ width: "170px" }}
                onClick={() => handleRetake()}
              >
                ReTake Exam
              </Button>

              <Button
                variant="contained"
                color="warning"
                style={{ width: "200px" }}
                onClick={() => setRouteAnswer(false)}
              >
                See Answer
              </Button>
              <Button
                variant="contained"
                color="success"
                style={{ width: "170px" }}
              >
                Go Dashboard
              </Button>
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  )
}
