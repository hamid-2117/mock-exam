import { makeStyles } from "@mui/styles"
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 1100,
  borderRadius: "20px",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  pb: 1,
}

const useStyles = makeStyles({
  navbar: {
    borderRadius: "17px 17px 0px 0px",
    height: "90px",
    margin: "0px auto",
    display: "grid",
    placeItems: "center",
    textAlign: "center",
    backgroundImage: "linear-gradient(#3960c3,#a36cfd)",
    "& h2": {
      margin: "0px",
      color: "white",
    },
  },
  header: {
    textAlign: "center",
    "& h2": {
      fontWeight: "600",
      fontSize: "30px",
    },
    "& h4": {
      fontWeight: "400",
      fontSize: "18px",
    },
  },
  grid: {
    padding: "0px 40px",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
  },
  result: {
    textAlign: "center",
  },
  btn: {
    padding: "20px 300px",
    display: "grid",
    placeItems: "center",
    gridTemplateColumns: "1fr 1fr 1fr",
    columnGap: "10px",
  },
})

export { useStyles, style }
