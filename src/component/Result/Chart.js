import React from "react";
import PieChart from "@bit/recharts.recharts.pie-chart";
import Pie from "@bit/recharts.recharts.pie";
// import Sector from "@bit/recharts.recharts.sector";
import Cell from "@bit/recharts.recharts.cell";

const data = [
  { name: "Group A", value: 400 },
  { name: "Group B", value: 300 },
  { name: "Group C", value: 300 },
  { name: "Group D", value: 200 },
];

const COLORS = ["#f90", "#43e0d0", "#f46aa0", "#9b6bf9"];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

export default function Chart({ result }) {
  const { total, totalScore, ...newObj } = result;
  const newArray = Object.entries(newObj).map(([k, v]) => ({ value: v }));
  return (
    <PieChart width={400} height={400} style={{ marginTop: "-100px" }}>
      <Pie
        data={newArray}
        cx={200}
        cy={200}
        labelLine={false}
        label={renderCustomizedLabel}
        outerRadius={120}
        fill="#8884d8"
        dataKey="value"
      >
        {data.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
    </PieChart>
  );
}
