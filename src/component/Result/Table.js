import * as React from "react"
import { styled } from "@mui/material/styles"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell, { tableCellClasses } from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Paper from "@mui/material/Paper"
import CircleIcon from "@mui/icons-material/Circle"

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}))

export default function CustomizedTables({ result }) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 200 }} aria-label="customized table">
        <TableHead></TableHead>
        <TableBody>
          <StyledTableRow>
            <StyledTableCell component="th" scope="row">
              <CircleIcon
                style={{
                  color: "#9b6bf9",
                  addingTop: "20px",
                  fontSize: "13px",
                }}
              />{" "}
              Total
            </StyledTableCell>
            <StyledTableCell align="right">{result.total}</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th" scope="row">
              <CircleIcon
                style={{
                  color: "#f90",
                  addingTop: "20px",
                  fontSize: "13px",
                }}
              />{" "}
              Unanswered
            </StyledTableCell>
            <StyledTableCell align="right">{result.unanswered}</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th" scope="row">
              <CircleIcon
                style={{
                  color: "#43e0d0",
                  addingTop: "20px",
                  fontSize: "13px",
                }}
              />{" "}
              Correct
            </StyledTableCell>
            <StyledTableCell align="right">{result.correct}</StyledTableCell>
          </StyledTableRow>
          <StyledTableRow>
            <StyledTableCell component="th" scope="row">
              <CircleIcon
                style={{
                  color: "#f46aa0",
                  addingTop: "20px",
                  fontSize: "13px",
                }}
              />{" "}
              Incorrect
            </StyledTableCell>
            <StyledTableCell align="right">{result.incorrect}</StyledTableCell>
          </StyledTableRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}
