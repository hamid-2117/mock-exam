import React, { useState, useEffect } from "react"
import { Button, Paper, Stack } from "@mui/material"
import CircleIcon from "@mui/icons-material/Circle"
import { useStyles, Item } from "./styling"
import { EMQbank } from "./EMQbank"

const QuestionBank = ({ questionLength, question, setSelected, selected }) => {
  const [btn, setBtn] = useState("none")
  const [data, setData] = useState(question)
  const [correctId, setCorrectId] = useState(false)

  useEffect(() => {
    const NewData = question.filter((data) => {
      if (btn === "correct") {
        if (data.ansId) {
          if (data.realanswer === data?.ansId) {
            return data
          }
        }
        if (data.type === "2") {
          if (data._id === correctId) {
            return data
          }
        }
      }
      if (btn === "incorrect") {
        if (data.type === "2") {
          if (data._id !== correctId) {
            return data
          }
        } else if (data.realanswer !== data?.ansId) {
          return data
        }
      }
      if (btn === "skipped") {
        return data.status === btn
      }
      if (btn === "none") {
        return data
      }
      return null
    })
    setData(NewData)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected, btn, question])

  const classes = useStyles()
  return (
    <Paper className={classes.mainpaper}>
      <div className={classes.header}>
        <Stack direction="row" spacing={0}>
          <Item>Question Bank</Item>
          <Item>{questionLength} Questions</Item>
        </Stack>
      </div>
      <div className={classes.main}>
        {data.map((data) => {
          const { id: _id } = data
          if (data.type === "3") {
            return (
              <EMQbank
                key={_id}
                _id={_id}
                setCorrectId={setCorrectId}
                data={data}
                classes={classes}
                setSelected={setSelected}
                selected={selected}
              />
            )
          }
          return (
            <div style={{ paddingLeft: "20px" }} key={_id}>
              <h5 className={classes.ques} onClick={() => setSelected(_id)}>
                <span
                  className={classes.leftColored}
                  style={
                    data.status === "attempted" &&
                    data.realanswer === data?.ansId
                      ? { border: "5px solid green" }
                      : data.status === "skipped"
                      ? { border: "5px solid orange" }
                      : { border: "5px solid red" }
                  }
                >
                  {"   "}
                </span>
                {"  "} Question {`${data.questionIndex + 1}`}
                <span className={classes.score}>
                  ({data.score} Score ) SBA{" "}
                </span>
              </h5>
            </div>
          )
        })}
      </div>
      <div style={{ position: "relative", marginTop: "20px" }}>
        <div className={classes.footer}>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "green",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("correct")}
            >
              <CircleIcon style={{ fontSize: "12px", marginBottom: "12px" }} />
              Correct
            </Button>
          </div>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "red",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("incorrect")}
            >
              <CircleIcon
                style={{
                  fontSize: "12px",
                  marginBottom: "12px",
                  textTransform: "capitalize",
                }}
              />
              Incorrect
            </Button>
          </div>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "orange",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("skipped")}
            >
              <CircleIcon style={{ fontSize: "12px", marginBottom: "12px" }} />
              Skipped
            </Button>
          </div>
        </div>
      </div>
    </Paper>
  )
}

export default QuestionBank
