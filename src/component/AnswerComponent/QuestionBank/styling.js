import { makeStyles } from "@mui/styles"
import { Paper } from "@mui/material"
import { styled } from "@mui/material/styles"
const useStyles = makeStyles((theme) => ({
  mainpaper: {
    position: "relative",
    "@media (min-width: 1400px)": {
      height: "900px",
    },
  },
  main: {
    overflowY: "auto",
    height: "700px",
    "&::-webkit-scrollbar-track": {
      WebkitBoxShadow: "inset 0 0 6px rgba(0,0,0,0.3)",
      backgroundColor: "linear-gradient(#3760c1,#7666e4,#a66bfe)",
    },
    "&::-webkit-scrollbar": {
      width: "9px",
      backgroundColor: "linear-gradient(#3760c1,#7666e4,#a66bfe)",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#cccccc",
      backgroundImage:
        "-webkit-linear-gradient(45deg,\n\trgba(255, 255, 255, .2) 25%,\n\t\t\t\t\t\t\t\t\t\t\t  transparent 25%,\n\t\t\t\t\t\t\t\t\t\t\t  transparent 50%,\n\t\t\t\t\t\t\t\t\t\t\t  rgba(255, 255, 255, .2) 50%,\n\t\t\t\t\t\t\t\t\t\t\t  rgba(255, 255, 255, .2) 75%,\n\t\t\t\t\t\t\t\t\t\t\t  transparent 75%,\n\t\t\t\t\t\t\t\t\t\t\t  transparent)",
    },
  },
  ques: {
    fontSize: "18px",
    cursor: "pointer",
  },
  secondGrid: {
    display: "grid",
    alignItems: "center",
    justifyContent: "end",
  },
  firstGrid: {
    display: "grid",
    gridTemplateColumns: "170px 300px 170px",
  },
  heading: {
    color: "white",
    fontSize: "23px",
    fontWeight: "500",
  },
  score: {
    fontSize: "16px",
    color: "#999999",
    fontWeight: "300",
  },
  label: {
    flexDirection: "column",
  },
  footer: {
    width: "100%",
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr ",
    "@media (max-width: 650px)": {
      padding: "0px",
    },
  },
  divfooter: {
    justifySelf: "center",
  },
  leftColored: {
    height: "100%",
    width: "20px",
    marginRight: "8px",
    backgroundColor: "black",
    "&:before": {
      content: " ",
      backgroundColor: "black",
    },
  },
}))
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(4),
  width: "100%",
  textAlign: "center",
  color: "black",
}))

export { useStyles, Item }
