import React, { useState, useEffect } from "react"

const EMQbank = ({ _id, data, classes, setSelected }) => {
  const [isTrue, setIsTrue] = useState(false)

  useEffect(() => {
    const answer = data.emq.every((dataa) => {
      return dataa.realanswer === dataa?.ansId
    })
    setIsTrue(answer)
  }, [])

  return (
    <div>
      <div style={{ paddingLeft: "20px" }} key={_id}>
        <h5 className={classes.ques} onClick={() => setSelected(_id)}>
          <span
            style={
              data.status === "attempted" && isTrue
                ? { border: "5px solid green" }
                : data.status === "skipped"
                ? { border: "5px solid orange" }
                : { border: "5px solid red" }
            }
          ></span>
          Question
          {` ${data.firstnum} to ${data.lastnum} `}
          <span className={classes.score}>({data.score} Score ) EMQ</span>
        </h5>
      </div>
    </div>
  )
}

export { EMQbank }
