import React from "react"
import { makeStyles } from "@mui/styles"
import { Button, Paper, Stack, useMediaQuery } from "@mui/material"
import { styled } from "@mui/material/styles"
import { Link } from "react-router-dom"
const useStyles = makeStyles({
  main: {
    height: "65px",
    backgroundImage: "linear-gradient(#3760c1,#7666e4,#a66bfe)",
    padding: "0px 30px",
    display: "grid",
    gridTemplateColumns: "1fr auto",
  },
  secondGrid: {
    display: "grid",
    alignItems: "center",
    justifyContent: "end",
  },
  firstGrid: {
    display: "grid",
    gridTemplateColumns: "170px 300px 170px",
    "@media (max-width: 1025px)": {
      gridTemplateColumns: "170px  170px",
    },
    "@media (max-width: 725px)": {
      gridTemplateColumns: "170px",
    },
  },
  heading: {
    color: "white",
    fontSize: "23px",
    fontWeight: "500",
  },
})
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: "white",
  backgroundColor: "#17a2b8",
}))

const Navbar = ({ setNewData, setRouteAnswer }) => {
  const classes = useStyles()
  const matches = useMediaQuery("(max-width:456px)")
  const matches1025 = useMediaQuery("(max-width:1025px)")
  const matches725 = useMediaQuery("(max-width:725px)")
  const matches551 = useMediaQuery("(max-width:551px)")
  return (
    <>
      <div className={classes.main}>
        {!matches && (
          <div className={classes.firstGrid}>
            <div style={{ margin: "auto 0px" }}>
              <Stack direction="row" spacing={2}>
                <Item>FCPS</Item>
                <Item>FCPS-1</Item>
              </Stack>
            </div>
            {!matches1025 && (
              <div>
                <h2 className={classes.heading}>
                  {" "}
                  Paper 1 For All Specialities
                </h2>
              </div>
            )}
            {!matches725 && (
              <div style={{ margin: "auto 0px" }}>
                <Item style={{ backgroundColor: "#28a745" }}>
                  Passing Marks: 64%
                </Item>
              </div>
            )}
          </div>
        )}
        <div className={classes.secondGrid}>
          <div>
            <Button
              variant="contained"
              style={{ backgroundColor: "#fd5252" }}
              onClick={() => {
                setNewData(null)
                setRouteAnswer(true)
              }}
            >
              {matches551 ? "Retake" : "Retake  Exam"}
            </Button>
            <Button
              variant="contained"
              style={{ backgroundColor: "#fd5252", marginLeft: "10px" }}
              // onClick={() => setOpen(true)}
            >
              {matches551 ? "Dashboard" : "Go to Dashboard"}
            </Button>
          </div>
        </div>
      </div>
    </>
  )
}

export default Navbar
