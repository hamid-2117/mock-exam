import { makeStyles } from "@mui/styles"
import { alpha, styled } from "@mui/material/styles"
import Button from "@mui/material/Button"

const useStyles = makeStyles({
  main: {
    padding: "30px",
  },
  question: {
    display: "grid",
    gridTemplateColumns: "120px auto",
    alignItems: "center",
    justifyContent: "start",
    "& h3": {
      fontWeight: "300",
    },
    "& h2": {
      fontWeight: "600",
    },
  },
  btnSection: {
    display: "grid",
    placeItems: "end",
    marginTop: "30px",
  },
  btn: {
    padding: "17px",
    marginLeft: "15px",
  },
  option: {
    marginBottom: "20px",
  },
  explain: {
    marginBottom: "300px",
  },
  childexplain: {
    minHeight: "100px",
    backgroundColor: "#f7f7f7",
  },
  btnquestion: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    "@media(max-width:495px)": {
      gridTemplateColumns: "1fr",
    },
  },
})
function isValidURL(string) {
  var res = string.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&//=]*)/g
  )
  return res !== null
}

function toLetters(num) {
  var mod = num % 26
  var pow = (num / 26) | 0
  var out = mod ? String.fromCharCode(64 + mod) : (pow--, "Z")
  return pow ? toLetters(pow) + out : out
}

const useStyleEMQ = makeStyles({
  main: {
    padding: "20px",
    marginBottom: "40px",
  },
  gridOptions: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
  },
  optionheading: {
    margin: "4px",
    fontWeight: "300",
  },
  questionheading: {
    fontWeight: "300",
    letterSpacing: "1px",
    marginTop: "17px",
    marginBottom: "34px",
    fontSize: "17px",
    "& span": {
      backgroundColor: "#17a2b8",
      padding: "3px",
      margin: "10px",
      color: "white",
      fontWeight: "400",
      borderRadius: "5px",
    },
  },
  instruction: {
    fontWeight: "300",
    letterSpacing: "1px",
    fontSize: "17px",
    "& span": {
      padding: "3px",
      margin: "10px",
      fontWeight: "900",
    },
  },
  explanation: {
    backgroundColor: "#f7f7f7",
    padding: "10px 10px",
    "& h4": {
      fontWeight: "300",
      fontSize: "17px",
    },
  },
  explanationimg: {
    width: "300px",
  },
  explanationpopimg: {
    width: "100%",
    borderRadius: "4px",
  },
  explanationpopbtn: {
    display: "grid",
    placeItems: "end",
  },
  explanationimgDiv: {
    padding: "0px 40px",
    paddingBottom: "30px",
  },
})

const StyledBtnFalse = styled(Button)(({ theme }) => ({
  width: 300,
  color: "black",
  backgroundColor: "#fd6363",
  "& .MuiSlider-thumb": {
    "&:hover, &.Mui-focusVisible": {
      backgroundColor: "#fd5252",
      // boxShadow: `0px 0px 0px 8px ${alpha(theme.palette.success.main, 0.16)}`,
    },
    "&.Mui-active": {
      boxShadow: `0px 0px 0px 14px ${alpha(theme.palette.success.main, 0.16)}`,
    },
  },
  "&: hover": {
    backgroundColor: "#e44a4a",
  },
}))

const StyledBtnTrue = styled(Button)(({ theme }) => ({
  width: 300,
  color: "black",
  backgroundColor: "#34bc30",
  "& .MuiSlider-thumb": {
    "&:hover, &.Mui-focusVisible": {
      boxShadow: `0px 0px 0px 8px ${alpha(theme.palette.success.main, 0.16)}`,
    },
    "&.Mui-active": {
      boxShadow: `0px 0px 0px 14px ${alpha(theme.palette.success.main, 0.16)}`,
    },
  },
  "&: hover": {
    backgroundColor: "#1ba217",
  },
}))
export {
  useStyles,
  isValidURL,
  useStyleEMQ,
  StyledBtnFalse,
  StyledBtnTrue,
  toLetters,
}
