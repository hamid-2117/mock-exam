import React, { useState } from "react"
import { useStyleEMQ } from "./styling"
import { Box, Fade, Backdrop, Button, IconButton, Modal } from "@mui/material"
import CloseIcon from "@mui/icons-material/Close"
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  borderRadius: "20px",
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
}

const Explanation = ({ selectedData }) => {
  const [open, setOpen] = useState(false)
  const classes = useStyleEMQ()
  const handleClick = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  return (
    <div>
      <h4>Explanation EMQ</h4>
      <div className={classes.explanation}>
        <h4>{selectedData.explanation}</h4>
        <Button onClick={handleClick}>
          <img
            src={selectedData.AMediaURL}
            className={classes.explanationimg}
            alt={selectedData.AMediaType}
          />
        </Button>
      </div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        onClose={handleClose}
        open={open}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <div className={classes.explanationpopbtn}>
              <IconButton onClick={handleClose}>
                <CloseIcon style={{ fontSize: "30px" }} />
              </IconButton>
            </div>
            <div className={classes.explanationimgDiv}>
              <img
                src={selectedData.AMediaURL}
                className={classes.explanationpopimg}
                alt={selectedData.AMediaType}
              />
            </div>
          </Box>
        </Fade>
      </Modal>
    </div>
  )
}

export default Explanation
