import React from "react"
import { Divider, Button, Paper } from "@mui/material"
import NextIcon from "@mui/icons-material/NavigateNext"
import BackIcon from "@mui/icons-material/ArrowBackIosNew"
import Dropdown from "./Dropdown"
import { useStyleEMQ, toLetters } from "./styling"

const Question = ({
  questions,
  setData,
  classess,
  handleNext,
  handlePrevious,
  matches,
  data,
}) => {
  const classes = useStyleEMQ()

  var Options = Object.keys(questions.options).map((key) => {
    return { [key]: questions.options[key] }
  })

  return (
    <Paper className={classes.main}>
      <div className={classess.btnquestion}>
        <div>
          <Button startIcon={<BackIcon />} onClick={handlePrevious}>
            {!matches ? "Move to Previous Question" : "Previous Question"}
          </Button>
        </div>
        <div style={{ justifySelf: "end" }}>
          <Button endIcon={<NextIcon />} onClick={handleNext}>
            {!matches ? "Move to Next Question" : "Next Question"}
          </Button>
        </div>
      </div>
      <h2>Options</h2>
      <Divider />
      <div className={classes.gridOptions}>
        {Options.map((data, index) => {
          const allOptions = Object.values(data)
          return (
            <h4 key={index} className={classes.optionheading}>
              ({toLetters(index + 1)}) {allOptions}
            </h4>
          )
        })}
      </div>
      <div>
        <h3 className={classes.instruction}>
          <span> Instruction : </span> {questions?.Description}
        </h3>
      </div>
      <div className={classes.question}>
        <h2>Question : </h2>
      </div>
      {Object.keys(questions.emq).map((v, index) => {
        return (
          <div key={index}>
            <Divider />
            <h3 className={classes.questionheading}>
              <span>
                Question{" "}
                {Object.values(questions.emq)[index].question.substring(0, 2)}
              </span>
              {Object.values(questions.emq)[index].question}
            </h3>
            <div>
              <Dropdown
                options={Options}
                toLetters={toLetters}
                _id={questions._id}
                indexOfQuestion={index}
                setData={setData}
                questions={questions}
                data={data}
              />
            </div>
          </div>
        )
      })}
    </Paper>
  )
}

export default Question
