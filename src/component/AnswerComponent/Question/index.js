import React, { useState, useEffect } from "react"
import EMQQuestion from "./EMQQuestion"
//Material UI Imports
import { Paper, Button, Divider, Radio, useMediaQuery } from "@mui/material"
import RadioGroup from "@mui/material/RadioGroup"
import FormControlLabel from "@mui/material/FormControlLabel"
import NextIcon from "@mui/icons-material/NavigateNext"
import BackIcon from "@mui/icons-material/ArrowBackIosNew"
import { useStyles, isValidURL } from "./styling"

const Question = ({
  question,
  setSelected,
  questionLength,
  selected,
  newData,
  attemptedArray,
  setNewData,
}) => {
  const classess = useStyles()
  const [questions, setQuestion] = useState({})
  const [selectedIndex, setSelectedIndex] = useState(null)
  const [answer, setAnswer] = useState(null)
  const matches = useMediaQuery("(max-width:756px)")
  useEffect(() => {
    const [NewQuestion] = question.filter((data, index) => {
      if (data._id === selected) {
        setSelectedIndex(index)
      }
      return data._id === selected
    })
    attemptedArray.map((dataa) => {
      if (selected === dataa.id) {
        setAnswer({ answer: dataa.answer, ansId: dataa.ansId })
      }
    })
    setQuestion(NewQuestion)
  }, [selected, question])
  //For Next Question

  const handleNext = () => {
    const newId = question.filter((data, index) => {
      if (index === selectedIndex + 1) {
        return data
      }
      if (selectedIndex === questionLength - 1 && index === 0) {
        return data
      }
      return null
    })
    setSelected(newId[0]._id)
  }

  //For Previous Question

  const handlePrevious = () => {
    const newId = question.filter((data, index) => {
      if (index === selectedIndex - 1) {
        return data
      }
      if (selectedIndex === 0 && index === 0) {
        return data
      }
      return null
    })
    setSelected(newId[0]._id)
  }
  if (questions === {}) {
    return <h4>Loading ...</h4>
  }
  if (questions?.type === "3") {
    return (
      <EMQQuestion
        questions={questions}
        classess={classess}
        handleNext={handleNext}
        handlePrevious={handlePrevious}
        matches={matches}
        selected={selected}
        data={newData}
        setData={setNewData}
      />
    )
  }
  return (
    <Paper className={classess.main}>
      <div className={classess.btnquestion}>
        <div>
          <Button startIcon={<BackIcon />} onClick={handlePrevious}>
            {!matches ? "Move to Previous Question" : "Previous Question"}
          </Button>
        </div>
        <div style={{ justifySelf: "end" }}>
          <Button endIcon={<NextIcon />} onClick={handleNext}>
            {!matches ? "Move to Next Question" : "Next Question"}
          </Button>
        </div>
      </div>
      <Divider />
      <div className={classess.question}>
        <h2>Question : </h2>
        <h3>{questions?.Description}</h3>
        <h2 style={{ marginLeft: "10px" }}>Options</h2>
      </div>
      <Divider />
      <div className={classess.option}>
        <RadioGroup
          aria-label="gender"
          name="controlled-radio-buttons-group"
          className={classess.grid}
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
          }}
          value={answer && answer?.answer}
        >
          <FormControlLabel
            name={"option01"}
            value={questions?.options?.option01 || ""}
            style={
              questions?.options?.option01 === questions?.answer?.option01
                ? { background: "#34bc30" }
                : answer?.ansId === "option01"
                ? { background: "#fd6363" }
                : null
            }
            control={<Radio />}
            label={questions?.options?.option01 || ""}
          />
          <FormControlLabel
            name={"option02"}
            style={
              questions?.options?.option02 === questions?.answer?.option02
                ? { background: "#34bc30" }
                : answer?.ansId === "option02"
                ? { background: "#fd6363" }
                : null
            }
            value={questions?.options?.option02 || ""}
            control={<Radio />}
            label={questions?.options?.option02 || ""}
          />
          <FormControlLabel
            name={"option03"}
            style={
              questions?.options?.option03 === questions?.answer?.option03
                ? { background: "#34bc30" }
                : answer?.ansId === "option03"
                ? { background: "#fd6363" }
                : null
            }
            value={questions?.options?.option03 || ""}
            control={<Radio />}
            label={questions?.options?.option03 || ""}
          />
          <FormControlLabel
            name={"option04"}
            value={questions?.options?.option04 || ""}
            style={
              questions?.options?.option04 === questions?.answer?.option04
                ? { background: "#34bc30" }
                : answer?.ansId === "option04"
                ? { background: "#fd6363" }
                : null
            }
            control={<Radio />}
            label={questions?.options?.option04 || ""}
          />
        </RadioGroup>
      </div>
      <div className={classess.explain}>
        <h3 style={{ margin: "4x 0px" }}>Explanation</h3>
        <Divider />
        <div className={classess.childexplain}>
          {questions?.exp && isValidURL(questions?.exp) ? (
            <img src={questions?.exp} alt={questions.exp} />
          ) : (
            <h4>{questions?.exp}</h4>
          )}
        </div>
      </div>
      <div className={classess.btnSection}>
        <div>
          <Button
            variant="contained"
            color="secondary"
            style={{ marginLeft: "10px" }}
            onClick={handlePrevious}
          >
            Previous
          </Button>
          <Button
            variant="contained"
            color="primary"
            style={{ marginLeft: "10px" }}
            onClick={handleNext}
          >
            Next
          </Button>
        </div>
      </div>
    </Paper>
  )
}

export default Question
