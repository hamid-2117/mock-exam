import React, { useState, useEffect } from "react"
import { StyledBtnFalse, StyledBtnTrue } from "./styling"
import Explanation from "./Explanation"
export default function BasicMenu({
  options,
  toLetters,
  indexOfQuestion,
  _id,
  questions,
  data,
}) {
  const [selected, setSelected] = useState(null)
  const [selectedKey, setSelectedKey] = useState(false)
  const [actualTrue, setActualTrue] = useState(false)
  const [selectedData, setSelectedData] = useState(false)

  useEffect(() => {
    if (options.length >= actualTrue) {
      setActualTrue(false)
    }
    if (options.length <= selected) {
      setSelected(null)
    }
  }, [actualTrue, options.length, selected])

  useEffect(() => {
    setSelected(null)
    if (data) {
      data.myAttempts.data.map((alldata) => {
        // console.log(indexOfQuestion)
        if (alldata._id === _id) {
          const keySelected = Object.keys(alldata.emq)[indexOfQuestion]
          setSelectedData(alldata.emq[keySelected])
          setSelectedKey(keySelected)
          if (
            alldata.emq[keySelected].answer ===
            alldata.emq[keySelected].answered
          ) {
            setActualTrue(
              Number(alldata.emq[keySelected].answer.substring(6, 8)) - 1
            )
          } else {
            setActualTrue(
              Number(alldata.emq[keySelected].answer.substring(6, 8)) - 1
            )
          }
        }
        return null
      })
    }
    if (selectedKey) {
      const [selectedAttempted] = data.myAttempts.attempted.filter((dataa) => {
        return dataa.id === _id
      })
      const answered = selectedAttempted.emq.filter((emqss) => {
        return emqss.id === selectedKey
      })

      if (answered) {
        options.map((data, index) => {
          if (Object.keys(data)[0] === answered[0]?.ansId) {
            setSelected(index)
          }
          return null
        })
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_id, selectedKey, actualTrue, selected])

  if (actualTrue >= options.length || selected >= options.length) {
    return <h3>Loading...</h3>
  }
  return (
    <>
      <div style={{ marginBottom: "30px", display: "grid", gap: "20px" }}>
        {actualTrue !== selected && (
          <>
            <StyledBtnFalse variant="outlined" style={{ maxWidth: "350px" }}>
              {selected == null
                ? "Select Answer"
                : `(${toLetters(selected + 1)}) ${
                    Object.values(options[selected])[0]
                  }`}
            </StyledBtnFalse>
          </>
        )}
        <StyledBtnTrue variant="outlined" style={{ maxWidth: "350px" }}>
          {actualTrue === false
            ? "Select Answer"
            : `(${toLetters(actualTrue + 1)}) ${
                Object.values(options[actualTrue])[0]
              }`}
        </StyledBtnTrue>
      </div>
      <div>{selectedData && <Explanation selectedData={selectedData} />}</div>
    </>
  )
}
