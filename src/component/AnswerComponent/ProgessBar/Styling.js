import { makeStyles } from "@mui/styles"
import LinearProgress from "@mui/material/LinearProgress"
import Typography from "@mui/material/Typography"
import { Box, Paper } from "@mui/material"
import { styled } from "@mui/material/styles"
const startTimer = (duration, display) => {
  var timer = duration,
    minutes,
    seconds
  setInterval(function () {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10)

    minutes = minutes < 10 ? "0" + minutes : minutes
    seconds = seconds < 10 ? "0" + seconds : seconds

    display = minutes + ":" + seconds

    if (--timer < 0) {
      timer = duration
    }
  }, 1000)
}

const useStyles = makeStyles({
  main: {
    padding: "15px",
    marginBottom: "15px",
  },
  progress: {
    "& h4": {
      marginBottom: "2px",
    },
  },
  timesection: {
    display: "grid",
    placeItems: "center",
    "& h3": {
      margin: "0px",
    },
    "& h2": {
      margin: "0px",
    },
  },
  grid: {
    display: "grid",
    width: "100%",
    columnGap: "20px",

    gridTemplateColumns: "1fr 1fr 1fr 1fr",
    "@media (max-width: 1022px)": {
      columnGap: "15px",
    },
    "@media (max-width: 700px)": {
      gridTemplateColumns: "1fr 1fr",
      gap: "10px",
      placeItems: "center",
    },
  },
})
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(3),
  paddingLeft: "70px",
  paddingRight: "70px",
  width: "120px",
  "@media (max-width: 1507px)": {
    padding: theme.spacing(2),
    paddingLeft: "40px",
    paddingRight: "40px",
    width: "90px",
  },
  "@media (max-width: 1367px)": {
    padding: theme.spacing(2),
    paddingLeft: "40px",
    paddingRight: "40px",
    width: "70px",
  },
  "@media (max-width: 1022px)": {
    padding: "8px",
    paddingLeft: "20px",
    paddingRight: "20px",
    width: "60px",
  },
  "@media (max-width: 860px)": {
    padding: theme.spacing(1),
    paddingLeft: "10px",
    paddingRight: "10px",
  },
  "@media (max-width: 700px)": {
    paddingLeft: "6px",
    paddingRight: "6px",
    width: "100px",
    placeItems: "center",
  },
  backgroundColor: "#fafafa",
  textAlign: "center",
  color: "black",
  "& div": {
    "& h5": {
      margin: "0px",
    },
    "& h4": {
      margin: "0px",
    },
  },
}))
const LinearProgressWithLabel = (props) => {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ width: "100%", mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  )
}

export { startTimer, useStyles, LinearProgressWithLabel, Item }
