import React from "react"
import { Paper } from "@mui/material"
import { useStyles, Item } from "./Styling"

const ProgressBar = ({ result }) => {
  const classes = useStyles()

  return (
    <Paper className={classes.main}>
      <div className={classes.grid}>
        <Item>
          <div>
            <h5>Correct</h5>
            <h4>{result.correct}</h4>
          </div>
        </Item>
        <Item>
          <div>
            <h5>Incorrect</h5>
            <h4>{result.incorrect}</h4>
          </div>
        </Item>
        <Item>
          <div>
            <h5>Unanswered</h5>
            <h4>{result.unanswered}</h4>
          </div>
        </Item>
        <Item>
          <div>
            <h5>Total Score</h5>
            <h4>{result.totalScore}</h4>
          </div>
        </Item>
      </div>
    </Paper>
  )
}

export default ProgressBar
