import React from "react"
import { useState, useEffect } from "react"
import { useStyles } from "./Styling"

const Timer = (props) => {
  const { initialMinute = 0, initialSeconds = 0 } = props
  const [minutes, setMinutes] = useState(initialMinute)
  const [seconds, setSeconds] = useState(initialSeconds)
  useEffect(() => {
    setMinutes(props.initialMinute)
    setSeconds(0)
  }, [props.initialMinute])
  useEffect(() => {
    let myInterval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1)
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(myInterval)
        } else {
          setMinutes(minutes - 1)
          setSeconds(59)
        }
      }
    }, 1000)
    return () => {
      clearInterval(myInterval)
    }
  })
  const classes = useStyles()

  return (
    <div>
      {minutes === 0 && seconds === 0 ? null : (
        <h1 className={classes.timer} style={{ margin: "0px" }}>
          {" "}
          {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
        </h1>
      )}
    </div>
  )
}

export default Timer
