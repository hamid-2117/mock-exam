import { makeStyles } from "@mui/styles"
import LinearProgress from "@mui/material/LinearProgress"
import Typography from "@mui/material/Typography"
import { Box, Paper } from "@mui/material"
import { styled } from "@mui/material/styles"
const startTimer = (duration, display) => {
  var timer = duration,
    minutes,
    seconds
  setInterval(function () {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10)

    minutes = minutes < 10 ? "0" + minutes : minutes
    seconds = seconds < 10 ? "0" + seconds : seconds

    display = minutes + ":" + seconds

    if (--timer < 0) {
      timer = duration
    }
  }, 1000)
}

const useStyles = makeStyles({
  main: {
    display: "grid",
    gridTemplateColumns: "5fr 2fr 2fr",
    padding: "15px",
    marginBottom: "15px",
    "@media (max-width: 490px)": {
      columnGap: "4px",
    },
  },
  progress: {
    "& h4": {
      marginBottom: "2px",
    },
  },
  timesection: {
    display: "grid",
    placeItems: "center",
    "& h3": {
      margin: "0px",
    },
    "& h2": {
      margin: "0px",
    },
  },
  answer: {
    margin: "auto 0px",
  },
  timer: {
    "@media (max-width: 1000px)": {
      fontSize: "20px",
    },
  },
})
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(3),
  paddingLeft: "50px",
  paddingRight: "50px",
  width: "70px",
  "@media (max-width: 1000px)": {
    padding: theme.spacing(1),
    paddingLeft: "20px",
    paddingRight: "20px",
    width: "60px",
  },
  textAlign: "center",
  color: "black",
  "& div": {
    "& h5": {
      margin: "0px",
    },
    "& h4": {
      margin: "0px",
    },
  },
}))
const LinearProgressWithLabel = (props) => {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ width: "100%", mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  )
}

export { startTimer, useStyles, LinearProgressWithLabel, Item }
