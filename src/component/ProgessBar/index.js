import React, { useState, useEffect } from "react"
import { Paper, Stack } from "@mui/material"
import useMediaQuery from "@mui/material/useMediaQuery"
import { LinearProgressWithLabel, useStyles, Item } from "./Styling"
import Timer from "./Timer"
const ProgressBar = ({ total, question, timer, retake }) => {
  const [progress, setProgress] = useState(10)
  const [scored, setScored] = useState(0)
  const matches = useMediaQuery("(max-width:370px)")
  const classes = useStyles()
  useEffect(() => {
    const { answered, attempted, lengthProgress } = question.reduce(
      (total, data) => {
        if (data.type === "3") {
          data.emq.map((dataa) => {
            if (dataa.status === "attempted") {
              total.answered += 1
            }
            return null
          })
          if (data.status === "attempted") {
            total.attempted += 1
          }
          total.lengthProgress += 1
        } else {
          if (data.status === "attempted") {
            total.answered += data.score
            total.attempted += 1
          }
          total.lengthProgress += 1
        }
        return total
      },
      { answered: 0, attempted: 0, lengthProgress: 0 }
    )
    setScored(answered)
    setProgress((attempted / lengthProgress) * 100)
  }, [question, total])

  return (
    <Paper className={classes.main}>
      {!matches && (
        <div className={classes.progress}>
          <h4>Overall Progress</h4>
          <LinearProgressWithLabel value={progress} />
        </div>
      )}

      <div className={classes.answer}>
        <Stack direction="row" spacing={1} style={{ margin: "auto 0px" }}>
          <Item>
            <div>
              <h5>Answered</h5>
              <h4>
                {scored}/{total.totalLength}
              </h4>
            </div>
          </Item>
          <Item>
            <div>
              <h5>Max Score</h5>
              <h4>{total.totalScore}</h4>
            </div>
          </Item>
        </Stack>
      </div>
      <div className={classes.timesection}>
        <h3> Time </h3>
        <h2>
          <Timer initialMinute={timer} retake={retake} />
        </h2>
      </div>
    </Paper>
  )
}

export default ProgressBar
