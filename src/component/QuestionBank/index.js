import React, { useState, useEffect } from "react"
import { Button, Paper, Stack } from "@mui/material"
import CircleIcon from "@mui/icons-material/Circle"
import { useStyles, Item } from "./styling"
import { EMQbank } from "./EMQbank"

const QuestionBank = ({ questionLength, setSelected, allData, selected }) => {
  const [btn, setBtn] = useState("none")
  const [data, setData] = useState(allData.myAttempts.attempted)
  useEffect(() => {
    const NewData = allData.myAttempts.attempted.filter((data) => {
      if (btn !== "none") {
        return data.status === btn
      }
      return data
    })
    setData(NewData)
  }, [selected, btn, allData.myAttempts.attempted])
  const classes = useStyles()
  return (
    <Paper className={classes.mainpaper}>
      <div className={classes.header}>
        <Stack direction="row" spacing={0}>
          <Item>Question Bank</Item>
          <Item>{questionLength} Questions</Item>
        </Stack>
      </div>
      <div className={classes.main}>
        {data.map((data) => {
          const { id } = data
          if (data.type === "3") {
            return (
              <EMQbank
                key={id}
                classes={classes}
                _id={id}
                data={data}
                setSelected={setSelected}
                selected={selected}
              />
            )
          }
          return (
            <div style={{ paddingLeft: "20px" }} key={id}>
              <h5 className={classes.ques} onClick={() => setSelected(id)}>
                <span>
                  <CircleIcon
                    style={
                      selected === id
                        ? { fontSize: "12px", color: "#731d1d" }
                        : data.status === "unanswered"
                        ? { fontSize: "12px", color: "#fd5252" }
                        : data.status === "attempted"
                        ? { fontSize: "12px", color: "#1cd316" }
                        : data.status === "skipped"
                        ? { fontSize: "12px", color: "#f90" }
                        : { fontSize: "12px" }
                    }
                  />{" "}
                  {"  "}
                </span>
                {"  "} Question {`${data.questionIndex + 1}`}
                <span className={classes.score}>({data.score} Score ) SBA</span>
              </h5>
            </div>
          )
        })}
      </div>
      <div style={{ position: "relative", marginTop: "20px" }}>
        <div className={classes.footer}>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "#fd5252",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("unanswered")}
            >
              <CircleIcon style={{ fontSize: "12px", marginBottom: "12px" }} />
              Unattempted
            </Button>
          </div>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "#1cd316",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("attempted")}
            >
              <CircleIcon
                style={{
                  fontSize: "12px",
                  marginBottom: "12px",
                  textTransform: "capitalize",
                }}
              />
              Attempted
            </Button>
          </div>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "#f90",
                textTransform: "capitalize",
              }}
              onClick={() => setBtn("skipped")}
            >
              <CircleIcon style={{ fontSize: "12px", marginBottom: "12px" }} />
              Skipped
            </Button>
          </div>
          <div className={classes.divfooter}>
            <Button
              style={{
                display: "grid",
                placeItems: "center",
                color: "#731d1d",
                textTransform: "capitalize",
              }}
            >
              <CircleIcon style={{ fontSize: "12px", marginBottom: "12px" }} />
              Current
            </Button>
          </div>
        </div>
      </div>
    </Paper>
  )
}

export default QuestionBank
