import React, { useState, useEffect } from "react"
import CircleIcon from "@mui/icons-material/Circle"

const EMQbank = ({ _id, data, classes, setSelected, selected }) => {
  const [unanswered, setUnanswered] = useState(true)
  useEffect(() => {
    data.emq.map((data) => {
      if (data.status === "unanswered") {
        setUnanswered(false)
      }
    })
  }, [data])
  return (
    <div>
      <div style={{ paddingLeft: "20px" }} key={_id}>
        <h5 className={classes.ques} onClick={() => setSelected(_id)}>
          <span>
            <CircleIcon
              style={
                selected === _id
                  ? { fontSize: "12px", color: "#731d1d" }
                  : unanswered
                  ? { fontSize: "12px", color: "#fd5252" }
                  : data.status === "skipped"
                  ? { fontSize: "12px", color: "#f90" }
                  : !unanswered
                  ? { fontSize: "12px", color: "#1cd316" }
                  : { fontSize: "12px" }
              }
            />
          </span>
          Question
          {` ${data.firstnum} to ${data.lastnum} `}
          <span className={classes.score}>({data.score} Score ) EMQ</span>
        </h5>
      </div>
    </div>
  )
}

export { EMQbank }
