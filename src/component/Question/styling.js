import { makeStyles } from "@mui/styles"
import Button from "@mui/material/Button"
import { alpha, styled } from "@mui/material/styles"
const useStyles = makeStyles({
  main: {
    padding: "30px",
  },
  question: {
    display: "grid",
    gridTemplateColumns: "120px auto",
    alignItems: "center",
    justifyContent: "start",
    "& h3": {
      fontWeight: "300",
    },
    "& h2": {
      fontWeight: "600",
    },
  },
  btnSection: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    placeItems: "end",
  },
  btn: {
    padding: "17px",
    marginLeft: "15px",
  },
  option: {
    marginBottom: "400px",
  },
  btnquestion: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    "@media(max-width:495px)": {
      gridTemplateColumns: "1fr",
    },
  },
})

const useStyleEMQ = makeStyles({
  main: {
    padding: "20px",
    marginBottom: "40px",
  },
  gridOptions: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
  },
  optionheading: {
    margin: "4px",
    fontWeight: "300",
  },
  questionheading: {
    fontWeight: "300",
    letterSpacing: "1px",
    marginTop: "17px",
    marginBottom: "34px",
    fontSize: "17px",
    "& span": {
      backgroundColor: "#17a2b8",
      padding: "3px",
      margin: "10px",
      color: "white",
      fontWeight: "400",
      borderRadius: "5px",
    },
  },
  instruction: {
    fontWeight: "300",
    letterSpacing: "1px",
    fontSize: "17px",
    "& span": {
      padding: "3px",
      margin: "10px",
      fontWeight: "900",
    },
  },
})
function isValidURL(string) {
  var res = string.match(
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_.~#?&//=]*)/g
  )
  return res !== null
}

const StyledBtn = styled(Button)(({ theme }) => ({
  width: 300,
  color: "black",
  "& .MuiSlider-thumb": {
    "&:hover, &.Mui-focusVisible": {
      boxShadow: `0px 0px 0px 8px ${alpha(theme.palette.success.main, 0.16)}`,
    },
    "&.Mui-active": {
      boxShadow: `0px 0px 0px 14px ${alpha(theme.palette.success.main, 0.16)}`,
    },
  },
}))
export { useStyles, useStyleEMQ, StyledBtn, isValidURL }
