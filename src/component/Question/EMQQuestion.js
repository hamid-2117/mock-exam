import React, { useState, useEffect } from "react"
import { Divider, Button, Paper } from "@mui/material"
import NextIcon from "@mui/icons-material/NavigateNext"
import BackIcon from "@mui/icons-material/ArrowBackIosNew"
import Dropdown from "./Dropdown"
import { useStyleEMQ } from "./styling"

const Question = ({
  questions,
  setData,
  classess,
  handleNext,
  handlePrevious,
  matches,
  lastId,
  selected,
  handleSkip,
  handleSubmit,
  setIsPQ,
  data,
  isPQ,
  newData,
}) => {
  const classes = useStyleEMQ()

  var Options = Object.keys(questions.options).map((key) => {
    return { [key]: questions.options[key] }
  })

  function toLetters(num) {
    var mod = num % 26
    var pow = (num / 26) | 0
    var out = mod ? String.fromCharCode(64 + mod) : (pow--, "Z")
    return pow ? toLetters(pow) + out : out
  }

  const [AttemptedBtn] = newData.myAttempts.attempted.filter((data) => {
    return data.id === questions._id
  })

  return (
    <Paper className={classes.main}>
      <div className={classess.btnquestion}>
        <div>
          <Button startIcon={<BackIcon />} onClick={handlePrevious}>
            {!matches ? "Move to Previous Question" : "Previous Question"}
          </Button>
        </div>
        <div style={{ justifySelf: "end" }}>
          <Button endIcon={<NextIcon />} onClick={handleNext}>
            {!matches ? "Move to Next Question" : "Next Question"}
          </Button>
        </div>
      </div>
      <h2>Options</h2>
      <Divider />
      <div className={classes.gridOptions}>
        {Options.map((data, index) => {
          const allOptions = Object.values(data)
          return (
            <h4 key={index} className={classes.optionheading}>
              ({toLetters(index + 1)}) {allOptions}
            </h4>
          )
        })}
      </div>
      <div>
        <h3 className={classes.instruction}>
          <span> Instruction : </span> {questions?.Description}
        </h3>
      </div>
      <div className={classes.question}>
        <h2>Question : </h2>
      </div>
      {Object.keys(questions.emq).map((v, index) => {
        return (
          <div key={index}>
            <Divider />
            <h3 className={classes.questionheading}>
              <span>
                Question{" "}
                {Object.values(questions.emq)[index].question.substring(0, 2)}
              </span>
              {Object.values(questions.emq)[index].question}
            </h3>
            <div>
              <Dropdown
                options={Options}
                toLetters={toLetters}
                _id={questions._id}
                indexOfQuestion={index}
                setData={setData}
                questions={questions}
                data={data}
                newData={newData}
                isPQ={isPQ}
              />
            </div>
          </div>
        )
      })}

      <div className={classess.btnSection}>
        <div style={{ justifySelf: "start" }}>
          {newData.myAttempts.courseType === 0 && (
            <>
              <Button
                variant="outlined"
                onClick={() => setIsPQ(() => true)}
                color="success"
              >
                Show
              </Button>
            </>
          )}
        </div>
        <div>
          <Button
            variant="contained"
            color="secondary"
            style={{ marginLeft: "10px" }}
            onClick={handlePrevious}
          >
            Previous
          </Button>
          {AttemptedBtn.status === "attempted" && selected !== lastId ? (
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              onClick={handleNext}
            >
              Next
            </Button>
          ) : selected === lastId ? (
            <Button
              variant="contained"
              style={{ marginLeft: "10px" }}
              color="info"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          ) : (
            <Button
              variant="outlined"
              style={{ marginLeft: "10px" }}
              color="warning"
              disabled={questions?.type === "attempted"}
              onClick={handleSkip}
            >
              Skip
            </Button>
          )}
        </div>
      </div>
    </Paper>
  )
}

export default Question
