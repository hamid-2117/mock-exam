import React, { useState, useEffect } from "react"
import Menu from "@mui/material/Menu"
import MenuItem from "@mui/material/MenuItem"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import { StyledBtn } from "./styling"
import Explanation from "../AnswerComponent/Question/Explanation"
export default function BasicMenu({
  options,
  toLetters,
  setData,
  indexOfQuestion,
  _id,
  data,
  newData,
  isPQ,
}) {
  const [anchorEl, setAnchorEl] = useState(null)
  const [selected, setSelected] = useState(null)
  const [selectedKey, setSelectedKey] = useState(false)
  const [selectedData, setSelectedData] = useState(false)
  const open = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleDataChange = (data, answered) => {
    const newData = data.myAttempts.attempted.map((allattempt) => {
      if (allattempt.id === _id) {
        const keySelected = allattempt.emq.map((data, index) => {
          if (index === indexOfQuestion) {
            return {
              ...data,
              ansId: answered,
              status: "attempted",
            }
          }
          return data
        })
        const isTrue = keySelected.every((data) => {
          return data.status === "attempted"
        })
        if (isTrue) {
          const newDataa = {
            ...allattempt,
            emq: keySelected,
            status: "attempted",
          }
          return newDataa
        }
        const newDataa = {
          ...allattempt,
          emq: keySelected,
        }
        return newDataa
      }
      return allattempt
    })

    return {
      ...data,
      myAttempts: {
        ...data.myAttempts,
        attempted: newData,
      },
    }
  }
  const handleBoth = (index, answered) => {
    setSelected(index)
    handleClose()
    setData((data) => {
      return handleDataChange(data, answered)
    })
  }

  useEffect(() => {
    setSelected(null)
    if (data) {
      data.map((alldata) => {
        if (alldata._id === _id) {
          const keySelected = Object.keys(alldata.emq)[indexOfQuestion]
          setSelectedData(alldata.emq[keySelected])
          setSelectedKey(keySelected)
        }
        return null
      })
    }
    if (selectedKey) {
      const answered = newData.myAttempts.attempted.filter((data) => {
        return _id === data.id
      })

      if (answered) {
        const answeredKey = answered[0].emq.filter((data) => {
          return data.id === selectedKey
        })
        if (answeredKey.length > 0) {
          options.map((data, index) => {
            if (Object.keys(data)[0] === answeredKey[0].ansId) {
              setSelected(index)
            }
            return null
          })
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_id, selectedKey])

  useEffect(() => {
    if (options.length <= selected) {
      setSelected(1)
    }
  }, [selected, options.length])

  if (selected >= options.length) {
    return <h3>Loading...</h3>
  }
  return (
    <>
      <div style={{ marginBottom: "30px" }}>
        <StyledBtn
          id="basic-button"
          aria-controls="simple-menu"
          aria-haspopup="true"
          variant="outlined"
          style={{ maxWidth: "350px" }}
          aria-expanded={open ? "true" : undefined}
          onClick={handleClick}
          endIcon={<KeyboardArrowDownIcon />}
        >
          {selected == null
            ? "Select Answer"
            : `(${toLetters(selected + 1)}) ${
                Object.values(options[selected])[0]
              } `}
        </StyledBtn>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          style={{ width: "100%" }}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          {options.map((data, index) => {
            const allOptions = Object.values(data)
            return (
              <MenuItem
                key={index}
                onClick={() => handleBoth(index, Object.keys(data)[0])}
              >
                ({toLetters(index + 1)}) {allOptions}
              </MenuItem>
            )
          })}
        </Menu>
      </div>
      <div>
        {isPQ && selectedData && <Explanation selectedData={selectedData} />}
      </div>
    </>
  )
}
