import React, { useState, useEffect } from "react"
import EMQQuestion from "./EMQQuestion"
//Material UI Imports
import { Paper, Button, Divider, Radio, useMediaQuery } from "@mui/material"
import RadioGroup from "@mui/material/RadioGroup"
import FormControlLabel from "@mui/material/FormControlLabel"
import NextIcon from "@mui/icons-material/NavigateNext"
import BackIcon from "@mui/icons-material/ArrowBackIosNew"
import { useStyles, isValidURL } from "./styling"

const Question = ({
  selected,
  question,
  setNewData,
  setSelected,
  questionLength,
  setOpen,
  newData,
}) => {
  const classess = useStyles()
  const [questions, setQuestions] = useState({})
  const [selectedIndex, setSelectedIndex] = useState(null)
  const [lastId, setLastId] = useState(false)
  const [answer, setAnswer] = useState(null)
  const [isPQ, setIsPQ] = useState(false)
  const matches = useMediaQuery("(max-width:756px)")
  useEffect(() => {
    const [NewQuestion] = question.filter((data, index, { length }) => {
      if (length - 1 === index) {
        setLastId(data._id)
      }
      if (data._id === selected) {
        setSelectedIndex(index)
      }
      return data._id === selected
    })
    if (NewQuestion?.answered) {
      setAnswer(NewQuestion.answered)
    }
    if (isPQ === true) {
      setIsPQ(false)
    }
    setQuestions(NewQuestion)
  }, [selected, question])

  useEffect(() => {
    if (answer) {
      setNewData((data) => {
        const updatedData = data.myAttempts.attempted.map((dataa) => {
          if (dataa.id === selected) {
            return {
              ...dataa,
              ansId: Object.keys(answer)[0],
              answer: Object.values(answer)[0],
              status: "attempted",
            }
          }
          return dataa
        })
        return {
          ...data,
          myAttempts: { ...data.myAttempts, attempted: updatedData },
        }
      })
      setQuestions((data) => {
        return { ...data, status: "attempted" }
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [answer])
  //For Next Question
  const handleNext = () => {
    const newId = question.filter((data, index) => {
      if (index === selectedIndex + 1) {
        return data
      }
      if (selectedIndex === questionLength - 1 && index === 0) {
        return data
      }
      return null
    })
    if (selected !== lastId) {
      setSelected(newId[0]._id)
    }
  }

  //For Previous Question

  const handlePrevious = () => {
    const newId = question.filter((data, index) => {
      if (index === selectedIndex - 1) {
        return data
      }
      if (selectedIndex === 0 && index === 0) {
        return data
      }

      return null
    })
    setSelected(newId[0]._id)
  }

  // For Handle Skip Funtionality
  const handleSkip = () => {
    setNewData((data) => {
      const updatedData = data.myAttempts.attempted.map((dataa) => {
        if (dataa.id === selected) {
          return {
            ...dataa,
            status: "skipped",
          }
        }
        return dataa
      })
      return {
        ...data,
        myAttempts: { ...data.myAttempts, attempted: updatedData },
      }
    })
    handleNext()
  }

  const handleSubmit = () => {
    setOpen(true)
  }

  if (questions?.type === "3") {
    return (
      <EMQQuestion
        questions={questions}
        data={question}
        newData={newData}
        classess={classess}
        setData={setNewData}
        matches={matches}
        handlePrevious={handlePrevious}
        handleNext={handleNext}
        handleSubmit={handleSubmit}
        lastId={lastId}
        selected={selected}
        isPQ={isPQ}
        setIsPQ={setIsPQ}
        handleSkip={handleSkip}
      />
    )
  }
  return (
    <Paper className={classess.main}>
      <div className={classess.btnquestion}>
        <div>
          <Button startIcon={<BackIcon />} onClick={handlePrevious}>
            {!matches ? "Move to Previous Question" : "Previous Question"}
          </Button>
        </div>
        <div style={{ justifySelf: "end" }}>
          <Button endIcon={<NextIcon />} onClick={handleNext}>
            {!matches ? "Move to Next Question" : "Next Question"}
          </Button>
        </div>
      </div>
      <Divider />
      <div className={classess.question}>
        <h2>Question : </h2>
        <h3>{questions?.Description}</h3>
        <h2 style={{ marginLeft: "10px" }}>Options</h2>
      </div>
      <Divider />
      <div className={classess.option}>
        <RadioGroup
          aria-label="gender"
          name="controlled-radio-buttons-group"
          className={classess.grid}
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
          }}
          value={answer && answer[Object.keys(answer)[0]]}
          onChange={(e) => setAnswer({ [e.target.name]: e.target.value })}
        >
          {["option01", "option02", "option03", "option04"].map((index) => {
            return (
              <FormControlLabel
                name={index}
                style={
                  isPQ && questions?.answer?.[index]
                    ? { backgroundColor: "#34bc30" }
                    : null
                }
                value={questions?.options?.[index] || ""}
                control={<Radio />}
                label={questions?.options?.[index] || ""}
              />
            )
          })}
        </RadioGroup>
        <div className={classess.childexplain}>
          {isPQ &&
            (questions?.exp && isValidURL(questions?.exp) ? (
              <img src={questions?.exp} alt={questions?.exp} />
            ) : (
              <h4>{questions?.exp}</h4>
            ))}
        </div>
      </div>
      <div className={classess.btnSection}>
        <div style={{ justifySelf: "start" }}>
          {newData.myAttempts.courseType === 0 && (
            <>
              <Button
                variant="outlined"
                onClick={() => setIsPQ(() => true)}
                color="success"
              >
                Show
              </Button>
            </>
          )}
        </div>
        <div>
          <Button
            variant="contained"
            color="secondary"
            style={{ marginLeft: "10px" }}
            onClick={handlePrevious}
          >
            Previous
          </Button>
          {questions?.status === "attempted" && selected !== lastId ? (
            <Button
              variant="contained"
              color="primary"
              style={{ marginLeft: "10px" }}
              onClick={handleNext}
            >
              Next
            </Button>
          ) : selected === lastId ? (
            <Button
              variant="contained"
              style={{ marginLeft: "10px" }}
              color="info"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          ) : (
            <Button
              variant="outlined"
              style={{ marginLeft: "10px" }}
              color="warning"
              disabled={questions?.status === "attempted"}
              onClick={handleSkip}
            >
              Skip
            </Button>
          )}
        </div>
      </div>
    </Paper>
  )
}

export default Question
