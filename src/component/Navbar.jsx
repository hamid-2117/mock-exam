import React from "react"
import { makeStyles } from "@mui/styles"
import { Button, Paper, Stack } from "@mui/material"
import { styled } from "@mui/material/styles"
import Model from "./Result"
import useMediaQuery from "@mui/material/useMediaQuery"
const useStyles = makeStyles({
  main: {
    height: "65px",
    backgroundImage: "linear-gradient(#3760c1,#7666e4,#a66bfe)",
    padding: "0px 30px",
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    "@media (max-width: 1000px)": {
      columnGap: "2px",
    },
  },
  secondGrid: {
    display: "grid",
    alignItems: "center",
    justifyContent: "end",
  },
  firstGrid: {
    display: "grid",
    gridTemplateColumns: "170px 300px 170px",
    "@media (max-width: 760px)": {
      gridTemplateColumns: "170px  170px",
    },
    "@media (max-width: 480px)": {
      gridTemplateColumns: "170px",
    },
  },
  heading: {
    color: "white",
    fontSize: "23px",
    fontWeight: "500",
  },
})
const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: "white",
  backgroundColor: "#17a2b8",
}))

const Navbar = ({
  open,
  setOpen,
  result,
  setRetake,
  setNewData,
  setRouteAnswer,
}) => {
  const classes = useStyles()
  const matches = useMediaQuery("(max-width:856px)")
  const matches780 = useMediaQuery("(max-width:780px)")
  const matches480 = useMediaQuery("(max-width:480px)")
  return (
    <>
      <Model
        open={open}
        setOpen={setOpen}
        result={result}
        setRetake={setRetake}
        setNewData={setNewData}
        setRouteAnswer={setRouteAnswer}
      />
      <div className={classes.main}>
        <div className={classes.firstGrid}>
          <div style={{ margin: "auto 0px" }}>
            <Stack direction="row" spacing={2}>
              <Item>FCPS</Item>
              <Item>FCPS-1</Item>
            </Stack>
          </div>
          {!matches780 && (
            <div>
              <h2 className={classes.heading}> Paper 1 For All Specialities</h2>
            </div>
          )}
          {!matches480 && (
            <div style={{ margin: "auto 0px" }}>
              <Item style={{ backgroundColor: "#28a745" }}>
                Passing Marks: 64%
              </Item>
            </div>
          )}
        </div>
        <div className={classes.secondGrid}>
          <div>
            <Button
              variant="contained"
              style={{ backgroundColor: "#fd5252" }}
              onClick={() => setOpen(true)}
            >
              {matches ? "Submit" : "Submit  Answer"}
            </Button>
          </div>
        </div>
      </div>
    </>
  )
}

export default Navbar
