import React, { useState, useEffect } from "react"
import { makeStyles } from "@mui/styles"
import Navbar from "./component/Navbar"
import Question from "./component/Question"
import QuestionBank from "./component/QuestionBank"
import ProgressBar from "./component/ProgessBar"
import { useLocalStorage } from "./hooks/useStorage"

const useStyles = makeStyles({
  main: {
    padding: "0px 20px",
    marginTop: "30px",
    backgroundColor: "#fafafa",
    display: "grid",
    gridTemplateColumns: "1fr 4fr",
    columnGap: "50px",
    "@media (max-width: 1000px)": {
      gridTemplateColumns: "350px auto",
      columnGap: "30px",
    },
    "@media (max-width: 850px)": {
      gridTemplateColumns: "260px auto",
      columnGap: "20px",
    },
    "@media (max-width: 650px)": {
      gridTemplateColumns: "1fr",
      columnGap: "20px",
    },
    "@media (max-width: 370px)": {
      gridTemplateColumns: "300px",
    },
  },
})
function App({ newData, setNewData, setRouteAnswer, propsData, dummyDataa }) {
  const classes = useStyles()
  const [selected, setSelected] = useState(null)
  const [attempted, setAttempted] = useState([])
  const [open, setOpen] = useState(false)
  const [result, setResult] = useState(false)
  const [retake, setRetake] = useState(true)
  const [timer, setTimer] = useLocalStorage("med-exam-timer", 0)

  const [total, setTotal] = useState({
    totalScore: 0,
    totalLength: 0,
  })

  useEffect(() => {
    if (retake) {
      const newTotal = propsData.reduce(
        (total, data) => {
          if (data.type === "3") {
            total.totalLength += Object.keys(data.emq).length
            total.totalScore += data.score
          }
          if (data.type === "1") {
            total.totalScore += data.score
            total.totalLength += 1
          }
          return total
        },
        {
          totalScore: 0,
          totalLength: 0,
        }
      )
      setTotal(newTotal)
      setSelected(propsData[0]._id)
      setRetake(false)
    }
    if (newData === null) {
      const attempted = propsData.map((alldata, index) => {
        if (alldata.type === "3") {
          const lengthArray = Object.getOwnPropertyNames(alldata.emq).length
          const lastnum = Number(
            alldata.emq[
              Object.keys(alldata.emq)[lengthArray - 1]
            ].question.substring(0, 3)
          )
          const firstnum = Number(
            Number(
              alldata.emq[Object.keys(alldata.emq)[0]].question.substring(0, 3)
            )
          )
          const emqq = Object.keys(alldata.emq).map((emqs) => {
            return {
              id: emqs,
              status: "unanswered",
            }
          })
          return {
            firstnum,
            lastnum,
            status: "unanswered",
            score: alldata.score,
            type: alldata.type,
            id: alldata._id,
            emq: emqq,
          }
        }

        return {
          id: alldata._id,
          status: "unanswered",
          type: alldata.type,
          questionIndex: index,
          score: alldata.score,
        }
      })
      const newDataa = {
        userId: dummyDataa.location.state.userId,
        courseId: dummyDataa.location.state.courseId,
        myAttempts: {
          max_score: dummyDataa.location.state.data.examDetails.max_score,
          courseType: dummyDataa.location.state.courseType,
          min_score: dummyDataa.location.state.data.examDetails.min_score,
          duration: dummyDataa.location.state.data.examDetails.max_time,
          attempted,
          data: propsData,
          courseType: dummyDataa.location.state.courseType,
          paperKey: dummyDataa.location.state.paperKey,
          sectionKey: dummyDataa.location.state.sectionKey,
          title: dummyDataa.location.state.courseTitle,
          totalTime: dummyDataa.location.state.data.examDetails.max_time,
        },
      }
      setNewData(newDataa)
      setTimer(30)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [propsData, retake, newData])

  useEffect(() => {
    if (open) {
      const result = newData.myAttempts.data.reduce(
        (total, data) => {
          if (data.type === "3") {
            if (data.type === "3") {
              newData.myAttempts.attempted.map((attemptedData) => {
                if (attemptedData.id === data._id) {
                  Object.keys(data.emq).map((emqss, index) => {
                    if (
                      attemptedData.emq[index].ansId === data.emq[emqss].answer
                    ) {
                      total.correct += 1
                    } else {
                      total.incorrect += 1
                    }
                    if (attemptedData.emq[index].status === "unanswered") {
                      total.unanswered += 1
                    }
                    total.total += 1
                  })
                  const isTrue = Object.keys(data.emq).every((emqss, index) => {
                    return (
                      attemptedData.emq[index].ansId === data.emq[emqss].answer
                    )
                  })
                  if (isTrue) {
                    total.totalScore += 1
                  }
                }
              })
            }
          }
          if (data.type === "1") {
            newData.myAttempts.attempted.map((attemptedData) => {
              if (attemptedData.id === data._id) {
                if (Object.keys(data.answer)[0] === attemptedData.ansId) {
                  total.correct += 1
                  total.totalScore += data.score
                } else {
                  total.incorrect += 1
                }
                if (
                  attemptedData.status === "unanswered" ||
                  attemptedData.status === "skipped"
                ) {
                  total.unanswered += 1
                }
              }
            })
            total.total += 1
          }
          return total
        },
        { unanswered: 0, correct: 0, incorrect: 0, total: 0, totalScore: 0 }
      )
      setResult(result)
    }
  }, [open, newData])

  if (!newData) {
    return <h3>Loading. . .</h3>
  }

  return (
    <>
      <Navbar
        open={open}
        setOpen={setOpen}
        result={result}
        setRetake={setRetake}
        setNewData={setNewData}
        setRouteAnswer={setRouteAnswer}
      />
      <div className={classes.main}>
        <QuestionBank
          questionLength={total.totalLength}
          allData={newData}
          setSelected={setSelected}
          selected={selected}
        />
        <div className={classes.answerection}>
          <ProgressBar
            total={total}
            answeredLength={attempted.length}
            question={newData.myAttempts.attempted}
            timer={timer}
            retake={retake}
          />
          <Question
            question={newData.myAttempts.data}
            setSelected={setSelected}
            questionLength={total.totalLength}
            selected={selected}
            allData={newData}
            setAttempted={setAttempted}
            setNewData={setNewData}
            setOpen={setOpen}
            newData={newData}
          />
        </div>
      </div>
    </>
  )
}

export default App
