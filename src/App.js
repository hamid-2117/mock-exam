import React, { useState, useEffect } from "react"
import Question from "./Question"
import Answered from "./Answered"
import { useLocalStorage } from "./hooks/useStorage"
import { props } from "./dummyData"
import "react-toastify/dist/ReactToastify.css"
import { useBeforeunload } from "react-beforeunload"
function MockQuestionss() {
  // const [newData, setNewData] = useLocalStorage("med-exam-question", null)
  const [newData, setNewData] = useState(null)
  const [RouteAnswer, setRouteAnswer] = useState(true)
  const propsData = props.location.state.data.exam
  const dummyDataa = props
  //reload detector

  useBeforeunload((event) => {
    event.preventDefault()
  })

  return (
    <>
      {RouteAnswer ? (
        <Question
          newData={newData}
          setNewData={setNewData}
          setRouteAnswer={setRouteAnswer}
          propsData={propsData}
          dummyDataa={dummyDataa}
        />
      ) : (
        <Answered
          propsData={propsData}
          newData={newData}
          setNewData={setNewData}
          setRouteAnswer={setRouteAnswer}
        />
      )}
    </>
  )
}

export default MockQuestionss
